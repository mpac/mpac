'''
Created on 24 Jan 2012

@author: marc
'''
from PackageBase import PackageBase
from database import mpacdb
from InstallScript import InstallScript
import os
from TermFunc import printTerm
from TermFunc import pflags
#globvar=1

class PackageRemover(PackageBase):

    def __init__(self, conf,options):
        '''
        Constructor
        '''
        self.conf=conf
        self.options=options
        self.packageList=[]
        self.packageCheckList=[]
        self.packagesToRemove=[]
        self.verboseMessages=[]
        self.blockedPackages={}  #key=user package : value = list of blocker packages
        self.badPackages=[]
        
        PackageBase.__init__(self, conf)  #super(PackageBase, self,conf).__init__()
        
        
    #check packages are installed and set the packagelist
    def SanityCheck(self,packageList,packagesNotInstalled):
        message=""
        db=mpacdb(self.conf["settings"]["mpac_database"])
        for package in packageList:
            if db.checkPackageIsInstalled(package):
                self.packageList.append(package)
            else:
                packagesNotInstalled.append(package)
                message+=package+ " is not installed\n"
        db.close()
        if len(message)>0:
            return message
        else:
            return True
    
    
    
    def GetPackageInfo(self):
        #What we do now depends on if the option to remove dependencies has been selected
        if self.options.clean_deps:
            #Attempt to clean (remove) dependencies
            self.__CheckRemoveWithDeps()
        else:
            #Do not clean (remove) dependencies
            self.__CheckRemoveWithoutDeps()
                    
             
             
             
    def __CheckRemoveWithoutDeps(self):
        db=mpacdb(self.conf["settings"]["mpac_database"])
        for package in self.packageList[:]:
            #print("checking "+package)
            rows = db.getPackagesThatDependOn(package)
            if rows != None:
                for row in rows:
                    meta=self.DbInstalledToMetaData(row)
                    if self.options.verbose:
                        self.verboseMessages.append(meta.name+" depends on "+package)
                    #add to blocker list
                    if package in self.blockedPackages:
                        if meta.name not in self.blockedPackages[package]:
                            self.blockedPackages[package].append(meta.name)
                    else:
                        self.blockedPackages[package]=[meta.name]                        
            else:
                #this is not required by any other package so can be removed
                if self.options.verbose:
                    self.verboseMessages.append(package+" is not required by any other package so can be removed")                
                self.packagesToRemove.append(package)
                    
        #are there any blocked packages
        if len(self.blockedPackages)>0:
            #there are blocked packages
            #is the blocker in the removal list
            for packageKey in self.blockedPackages.copy().keys():
                for blocker in self.blockedPackages.copy()[packageKey]:
                    if blocker in self.packagesToRemove:
                        #the blocker package is now in the remove list so it is no longer blocking
                        self.blockedPackages[packageKey].remove(blocker)
                #if all the blocker items have been deleted the package can be removed
                if len(self.blockedPackages[packageKey])==0:
                    del(self.blockedPackages[packageKey])
                    self.packagesToRemove.append(packageKey)
            #Ok so now if there are still blocked packages the package must not be removed
            #either way pass control back for reporting and the instigate the next stage
            if self.blockedPackages:
                db.close()
                return False
            else:
                db.close()
                return True
             
    
    
    def __CheckRemoveWithDeps(self):
        
        #call this to see if any packages are blocking the user provided packages
        #will use for comparison later
        self.__CheckRemoveWithoutDeps() 
        
        #create the initial check list of each user package plus its dependencies
        db=mpacdb(self.conf["settings"]["mpac_database"])
        for package in self.packageList:
            self.packageCheckList.append(package) #add the main package            
            packageRow=db.getInstalledPackage(package)
            if packageRow != None:            
                pMeta=self.DbInstalledToMetaData(packageRow)
                for dep in pMeta.depends: #add it's dependnecies                
                    depInfo=self.getDepInfo(dep)
                    if depInfo.name not in self.packageCheckList:
                        self.packageCheckList.append(depInfo.name) # add to checklist for next pass
        db.close()
        self.__DoPackageCheck()
        
        #print(self.blockedPackages) #DEBUG ONLY *********
        
        
                         
            
    #recursive function
    #Iterate though package (check) list if package does not depend on any other package add to list
    #of packages to remove, then get it's dependencies and add each one to the check list.
    #if dependencies cannot be removed add to bad package list.        
    def __DoPackageCheck(self):
        #global globvar
        #globvar = globvar+1
        #print(globvar)
        repeat=False
        db=mpacdb(self.conf["settings"]["mpac_database"])
        for package in self.packageCheckList[:]:
            rows = db.getPackagesThatDependOn(package)
            canRemove=True
            if rows != None:                
                for row in rows:
                    meta=self.DbInstalledToMetaData(row)                             
                    if self.options.verbose:
                        self.verboseMessages.append(meta.name+" depends on "+package)
                    
                    #if the dependnency is already in packages to remove then it can be removed else it cannot so
                    #at that point we give up checking
                    if meta.name not in self.packagesToRemove: 
                        canRemove=False                            
                if package in self.packageCheckList:  
                    self.packageCheckList.remove(package) #checked so remove from checklist
                
                if canRemove:
                    self.__AddNewDepsToPackageCheckList(package)
                    if package not in self.packagesToRemove:
                        self.packagesToRemove.append(package)
                    repeat=True
                else:
                    if package not in self.badPackages:
                        self.badPackages.append(package)                
            else:
                #not required by any other package                
                if package in self.packageCheckList:
                    self.packageCheckList.remove(package) #checked so remove from checklist
                if package not in self.packagesToRemove:
                    self.packagesToRemove.append(package) 

        if repeat:
            #call this function again (recursive)
            self.__DoPackageCheck()
        db.close()        
                
  
  
    
    def __AddNewDepsToPackageCheckList(self,package):
        db=mpacdb(self.conf["settings"]["mpac_database"])
        packageRow=db.getInstalledPackage(package)
        if packageRow != None:            
            pMeta=self.DbInstalledToMetaData(packageRow)
            #print(pMeta.name+ " install reason "+pMeta.installReason)
            if pMeta.installReason == "Installed as a dependency for another package":                
            #if 1==1: #debug                        
                #print("PACKAGE "+pMeta.name)
                for dep in pMeta.depends:                
                    depInfo=self.getDepInfo(dep)
                    #print("   ADDING "+depInfo.name)
                    if depInfo.name not in self.packageCheckList:
                        self.packageCheckList.append(depInfo.name) # add to checklist for next pass
        db.close()
  
  
    
    #If the given package name if found to be a blocker then free it and add to the packages to remove list
    #if found to be a blocker overal result is True
    #We will also see if the blocked package now has no blockers, if so we can add the blocked packages to 
    #the remove list.
    def __checkIfPackageCanBeUnblocked(self,name):
        result=False
        for blockedPackage in self.blockedPackages.keys():
            for blocker in self.blockedPackages[blockedPackage]:
                if blocker==name:
                    result=True
                    if self.options.verbose:
                        self.verboseMessages.append(blocker+ " no longer blocking "+blockedPackage)
                    self.blockedPackages[blockedPackage].remove(blocker)
            if not self.blockedPackages[blockedPackage]:
                if self.options.verbose:
                    self.verboseMessages.append(blockedPackage+ " is now unblocked")
                del(self.blockedPackages[blockedPackage])
                if blockedPackage not in self.packagesToRemove:
                    self.packagesToRemove.append(blockedPackage) 
        return result
  
    
    #do a final check - if packages do not exist install table check provider packages
    def CheckRemoveList(self):
        db=mpacdb(self.conf["settings"]["mpac_database"])
        for package in self.packagesToRemove[:]:
            if db.checkPackageExists(package)==False:
                #packages not found in installed package table, check if another package provides this
                rows=db.getProviderInstalledPackageFor(package)
                if rows != None:
                    for row in rows: #found provider package
                        provName=row[0]
                        #can check this provider package can be removed
                        depRows=db.getPackagesThatDependOn(provName)
                        if depRows != None:
                            for depRow in depRows:
                                canRemoveDep=False
                                depMeta=self.DbInstalledToMetaData(depRow)
                                #depMeta can only be removed if it is already in packages to remove
                                #or is a blocker package
                                if depMeta.name in self.packagesToRemove:
                                    canRemoveDep=True
                                if not canRemoveDep:
                                    #check blocked packages
                                    if self.__checkIfPackageCanBeUnblocked(depMeta.name):
                                        canRemoveDep=True
                                
                                if canRemoveDep:
                                    if depMeta.name not in self.packagesToRemove:
                                        self.packagesToRemove.append(depMeta.name) #add the real package name
                                    if package in self.packagesToRemove: #remove the original provider
                                        self.packagesToRemove.remove(package)
                                       
                                        
                        else:
                            #nothing depends on it so it can be removed
                            if self.options.verbose:
                                self.verboseMessages.append("proivider package "+provName+ " that provides "+package+" can be removed")
                            if provName not in self.packagesToRemove:
                                self.packagesToRemove.append(provName) #add the real package name
                            if package in self.packagesToRemove: #remove the original provider
                                self.packagesToRemove.remove(package)
    
    
    #helper function
    def __removeFile(self,file):
        if self.options.verbose:
            self.verboseMessages.append("Removing file: "+file)
        os.remove(file)
                                        

    #go through packages list get the file list and actually delete the packages from
    #the system
    def RemovePackages(self):
        errorMessage=[]
        
        #clear verbose messages as earlier ones should already been dislayed to the user at this stage.
        self.verboseMessages=[] 
        db=mpacdb(self.conf["settings"]["mpac_database"])
        for package in self.packagesToRemove:            
            self.__CheckPreRemoveScript(package)
            paths=[]
            #get the file list
            fileList=db.getPackageFiles(package)
            if fileList==None:
                errorMessage.append("Error: could not get package files for "+package)
            else:
                for file in fileList:
                    file=file[0] #n.b file[1] is MD5 hash
                    #print("FILE "+file)
                    if os.path.dirname(file) not in paths: #strip  out path from file name
                        paths.append(os.path.dirname(file)) #add to list will try to remove later (if empty)
                                            
                    #does file exist on the system
                    if os.path.isfile(file): #check for file and sym links
                        if os.path.islink(file):
                            #does the link taget exist
                            linkTarget=os.readlink(file)
                            if not os.path.exists(linkTarget):
                                #the link target does not exist
                                if self.options.verbose:
                                    self.verboseMessages.append(file+" is a broken symlink "+linkTarget+" does not exist")
                                #so just delete the link
                                self.__removeFile(file)
                            else: #can remove the link target and file - only if target does not belong to another package
                                canRemoveTarget=False
                                owner = db.getFileOwner(linkTarget)
                                if owner==None:
                                    #we could not find the owner so leave it - may adopt the --force option later!
                                    errorMessage.append("Could not get owner for "+linkTarget+" (sym link destination) so will not delete" )
                                elif owner != package:
                                    errorMessage.append("the sym link target "+linkTarget+" is owned by "+owner)
                                else:
                                    canRemoveTarget=True #must be owned by current package

                                if canRemoveTarget:
                                    if self.options.verbose:
                                        self.verboseMessages.append("Removing sym link: "+linkTarget)
                                    os.remove(linkTarget)
                                self.__removeFile(file)
                                                                    
                                if self.options.verbose:
                                    self.verboseMessages.append("Removing sym link: "+file)                                
                                os.remove(linkTarget)
                                self.__removeFile(file)
                        else:
                            #regular file
                            self.__removeFile(file)
                    else:
                        #file does not exist on the system
                        if self.options.verbose:
                            self.verboseMessages.append(file +" does not exist on the system")
            
            #now we can try to remove the empty directories                
            if paths:
                self.__RemoveEmptyPaths(paths)
            
            self.__CheckPostRemoveScript(package)
            
            #update the database
            db.removePackage(package)
            db.removePackageFiles(package)
                
        db.close()
        
        return errorMessage
     
    def __CheckPreRemoveScript(self,packageName):            
        #process the install script
        attrib=pflags()
        db=mpacdb(self.conf["settings"]["mpac_database"])
        row = db.getInstalledPackage(packageName)
        meta=self.DbInstalledToMetaData(row)   
        db.close
        if meta.installScript != "None":
            script=InstallScript()
            script.LoadScript(meta.installScript)
            scriptResult=True        
            if script.hasPreRemove:
                scriptResult=script.RunPreRemove()
            if not scriptResult:        
                #log warning
                printTerm("red","Warning: there was an error running the pre-remove script for "+meta.name,attrib.bold)
             
    def __CheckPostRemoveScript(self,packageName):            
        #process the install script
        attrib=pflags()
        db=mpacdb(self.conf["settings"]["mpac_database"])
        row = db.getInstalledPackage(packageName)
        meta=self.DbInstalledToMetaData(row)   
        db.close
        if meta.installScript != "None":
            script=InstallScript()
            script.LoadScript(meta.installScript)
            scriptResult=True        
            if script.hasPostRemove:
                scriptResult=script.RunPostRemove()

            if not scriptResult:        
                #log warning
                printTerm("red","Warning: there was an error running the post-remove script for "+meta.name,attrib.bold)
 
        
    #only remove empyty directoies in given path   
    def __RemoveEmptyPaths(self,paths):
        #sort the list so longest paths are first
        paths=sorted(paths,key=len,reverse=True)
        
        for path in paths:
            if self.options.verbose:
                self.verboseMessages.append("Checking path %s"%path)
            try:
                if os.path.exists(path):
                    os.removedirs(path)
            except OSError:
                if self.options.verbose:
                    self.verboseMessages.append(path+ " not empty")