'''
Created on 24 Jan 2012

@author: marc
'''

from PackageRemover import PackageRemover
import UserInput
from database import mpacdb
from TermFunc import printTerm
from TermFunc import pflags


def DoRemove(conf,packageList,options):
    remover=PackageRemover(conf,options)   
    packagesNotInstalled=[] 
    sanityCheck=remover.SanityCheck(packageList,packagesNotInstalled)
    attrib=pflags()
        
    if  sanityCheck != True:        
        printTerm(attrib.COL_ERR_WARN,sanityCheck,attrib.bold)        
        if len(packagesNotInstalled)>0:
            db=mpacdb(conf["settings"]["mpac_database"]) 
            for packageNotInstalled in packagesNotInstalled:
                #packageList.remove(missingPackage) #remove from package list
                printTerm(attrib.COL_INFO,">> Checking group for "+packageNotInstalled,0)
                groupResults=db.getInstalledGroupPackageNames(packageNotInstalled)
                if groupResults==None:
                    printTerm(attrib.COL_ERR_WARN,"No group for "+packageNotInstalled,attrib.bold)
                else:
                    printTerm(attrib.COL_INFO,"Found group for "+packageNotInstalled,attrib.bold)
                    for name in groupResults:
                        remover.packageList.append(name[0])
        
    else:
        if options.verbose:
            printTerm(attrib.COL_OK,"Sanity check OK",attrib.bold)
        
    if len(remover.packageList)==0:
        printTerm(attrib.COL_MAIN_BODY,"No packages to remove",attrib.bold)
        return
    else:
                  
        remover.GetPackageInfo()
           
        for package in remover.blockedPackages.keys():
            for blocker in remover.blockedPackages[package]:
                printTerm(attrib.COL_ERR_WARN,">> "+package+" is required by "+blocker,attrib.bold)
            
        if remover.packagesToRemove:                                            
            
            '''
            Do a final check to see if the packages to remove are actual packages
            rather than provider packages (not easy to test !!)
            
            for e.g. openjdk6 provides java-environment
            
            THIS WOULD BE SLOWER BUT MORE EFFECTIVE IF DONE IN THE DEP PROCESSING IN REMOVER!!!!!!!!!
            
            '''
            remover.CheckRemoveList() #final sanity check - check provider packages                    
        
        for message in remover.verboseMessages:
            printTerm(attrib.COL_INFO+">> "+message,0)             
            print("")
                
        if remover.packagesToRemove:
            printTerm(attrib.COL_HEADER,"\nThe following packages will be removed (%i):"%len(remover.packagesToRemove),attrib.bold)
            for package in remover.packagesToRemove:
                printTerm(attrib.COL_MAIN_BODY,">> "+package,attrib.bold)
            
            err=[]
            if UserInput.GetYesNoChoice("\nRemove?")=='y':
                #Now actually remove the packages
                err=remover.RemovePackages()
                                    
            
            if options.verbose:
                for msg in remover.verboseMessages:
                    printTerm(attrib.COL_INFO,msg,0)
            
            for msg in err:
                    printTerm(attrib.COL_ERR_WARN,msg,attrib.bold)

        else:
            printTerm(attrib.COL_MAIN_BODY,"**No Packages to remove**",attrib.bold)
                
    