'''
Created on 11 Oct 2011

@author: marc
'''

#Arch Linux meta data object
class MetaData:
    def __init__(self):
        self.name="null"
        self.version="null"
        self.description="null"
        self.packageFileName="null"
        self.groups=[]
        self.csize=0
        self.isize=0;
        self.md5sum="null"
        self.url="null"
        self.license=[]
        self.arch="null"
        self.replaces=[]
        self.optionalDependencies=[]
        self.requiredBy=[]
        self.depends=[]
        self.makedepends=[] #only used for AUR 
        self.conflicts=[]
        self.provides=[]
        self.installDate="null"
        self.installReason="null"
        self.installScript="null"
        self.distro="null"
        self.repository="null"
        self.isUpgrade=False
        self.isReinstall=False
        self.pkgRelease="null"
        
        
    def asList(self):
        listItems=[self.name,self.version,self.description,self.packageFileName,self.groups,
        self.csize,self.isize,self.md5sum,self.url,self.license,self.arch,self.replaces,
        self.optionalDependencies,self.requiredBy,self.depends,self.makedepends,self.conflicts,self.provides,
        self.installDate,self.installReason,self.installScript,self.distro,self.repository,
        self.isUpgrade,self.isReinstall,self.pkgRelease]
        return listItems
        