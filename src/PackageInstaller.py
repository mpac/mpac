'''
Created on 10 Oct 2011

@author: marc

Will have public functions to:
    resolve dependencies
    check package replacements
    set install reason
    install packages
    install local packages
    install group 
    

'''

from database import mpacdb 
from PackageBase import PackageBase
from DependencyResolver import DependencyResolver
import download
#import tarfile
import tar
import os
import util
import shutil
import constants
from InstallScript import InstallScript
import UserInput
import datetime
import fileTableCols
import packageTableCols
from MetaData import MetaData
from TermFunc import printTerm
from TermFunc import pflags


#Must remember to break the functions down so calling function can go step by step
class PackageInstaller(PackageBase):
    
    def __init__(self, conf,options):
        self.packageNameList = [] # a list of package names fron the user
        self.packageMetaList = [] # all meta data for package list
        self.newPackages = [] # a list of packages for installation to satisfy dependencies 
        #self.hasError=False
        #self.ErrorMessage=""
        self.conf = conf
        self.unresolved = []       # list of unresolved dependencies
        self.conflictPackages = {} # {<package>:<conflictsWith[]>}
        self.fileConflictInfo = [] # list of strings for each file conflict found
        self.optionalDepsDict = {} # dictionary of key=package name, value=opt dependency list
        self.configFilesForDB = [] #list of new config files [<etc file>,<hash>]
        self.options=options
        
        
        PackageBase.__init__(self, conf)  #super(PackageBase, self,conf).__init__()
        
        
            
    def ResolveAll(self):  
        #Will use DependencyResolver class here
        if self.options.local:
            self.__getLocalPackageMeta() #parse local package and populate packageMetaList
        else:    
            self.__getPackageMeta() #get all meta data from repos - populate packageMetaList
        
        resolver = DependencyResolver(self.conf,self.options)
        resolver.ResolveDependencies(self.packageMetaList)
        self.newPackages = resolver.GetNewPackages()
        self.unresolved = resolver.GetUnresolved()
        #log("finished resolving deps") #debug line
        
        if len(self.unresolved) == 0:
            self.__SetInstallInfo()
        

    def __SetInstallInfo(self):
        db = mpacdb(self.conf["settings"]["mpac_database"])
        #all packages in the newPackages list are being installed as dependencies
        for meta in self.newPackages:
            #this will update the meta data Ok ... I've checked!!!!!
            meta.installReason = "Installed as a dependency for another package"
            #now lets see if we are upgrading or 
            if db.checkPackageIsInstalled(meta.name):
                #check if version matches if yes then is reinstall else is upgrade
                row = db.getPackageVersion(meta.name)
                installedVersion = row[0]
                if installedVersion == meta.version:
                    meta.isReinstall = True
                else:
                    meta.isUpgrade = True                    
            
            
        #now sort out the main package list        
        for meta in self.packageMetaList:
            #first check if installed
            if db.checkPackageIsInstalled(meta.name):
                #check is version matches if yes then is reinstall else is upgrade
                row = db.getInstalledPackage(meta.name)
                installed = self.DbInstalledToMetaData(row)
                if installed.version == meta.version:
                    meta.isReinstall = True
                else:
                    meta.isUpgrade = True
                    
                #as the package is installed we need to retain the original install reason
                if self.options.asdependency: #check the user has forced install as dependency
                    meta.installReason="Installed as a dependency for another package"
                else:    
                    meta.installReason = installed.installReason
                                               
            else:
                #package is not already installed
                if self.options.asdependency: #check the user has forced install as dependency
                    meta.installReason = "Installed as a dependency for another package"
                else:   
                    meta.installReason = "Explicitly installed"
                
        db.close()
                
                
    '''------------------------------------------------------------------------
    Compile a dictionary of metareplacement packages.
    ReplacementPackages{<PackageToBeReplaced>:<ReplacementPackage>}
    check joined packageMetalist and newPackages
    -----------------------------------------------------------------------'''
    def CheckReplacementPackages(self):
        replacementPackages = {}
        allPackages = self.packageMetaList + self.newPackages
        #copyOfAllPackages=allPackages[:]
        db = mpacdb(self.conf["settings"]["mpac_database"])
        for packageMeta in allPackages:
            #check the replace list in meta data to see if these packages are installed
            for replace in packageMeta.replaces:
                if db.checkPackageIsInstalled(replace):
                    #it is installed so add to replace list
                    if replace in replacementPackages:
                        #replacementPackages[replace](packageMeta.name)
                        raise Exception("Cannot handle mutliple replaces\n" + replace + "is already in replacement list")
                    else:
                        replacementPackages[replace] = packageMeta.name
                        
        db.close()
        if len(replacementPackages) == 0:
            return None
        else:
            return replacementPackages    
  
    
    
    '''-----------------------------------------------------------------------
    Check for packages conflicts. First check the install list, if not in 
    (to be) install list then check installed packages.
    If the conflict contains a version then check the version
    populates self.conflictPackages{<name>:<[conflict1,conflict2,conflict3 etc...]>}
    returns true if conflicts found else returns false
    -----------------------------------------------------------------------'''
    def CheckPackageConflicts(self,packagesMarkedForRemove): 
        self.fileConflictInfo=[]
        hasConflicts = False
        allPackages = self.packageMetaList + self.newPackages
        #allPackagesCheck=allPackages[:] #copy the list
        db = mpacdb(self.conf["settings"]["mpac_database"])
        for meta in allPackages:
            for conflictsWith in meta.conflicts:
                if conflictsWith == "None":
                    break # no conflicts
                conflictInfo = self.getDepInfo(conflictsWith) #conflicts may have a version 
                if conflictInfo.name in packagesMarkedForRemove:
                    break #conflict is due to be removed so not a problem
                
                #First check install list if not in install list then search installed packages
                if self.IsNameInMetaList(allPackages, conflictInfo.name):
                    #found in the install list                    
                    if conflictInfo.version == None:
                        #conflict found
                        hasConflicts = True
                        if meta.name in self.conflictPackages:
                            self.conflictPackages[meta.name].append(conflictInfo.name)
                        else:
                            self.conflictPackages[meta.name] = [conflictInfo.name]  
                    else:
                        #has version so check name and verison
                        #pull the version from the install list that it conflicts wirh
                        instPkgMeta = allPackages[self.GetIndexOfNameInMetaList(allPackages, conflictInfo.name)]
                        if self.__isConflict(conflictInfo, instPkgMeta.version):
                            hasConflicts = True
                            if meta.name in self.conflictPackages:
                                self.conflictPackages[meta.name].append(conflictInfo.name)
                            else:
                                self.conflictPackages[meta.name] = [conflictInfo.name]  
                else:
                    #not in install list so check installed packages
                    if db.checkPackageIsInstalled(conflictInfo.name):
                        if conflictInfo.version == None:
                            #any version so is a conflict
                            hasConflicts = True
                            if meta.name in self.conflictPackages:
                                self.conflictPackages[meta.name].append(conflictInfo.name)
                            else:
                                self.conflictPackages[meta.name] = [conflictInfo.name]  
                        else:
                            #need to check version
                            row = db.getInstalledPackage(conflictInfo.name)
                            instMeta = self.DbInstalledToMetaData(row)
                            if self.__isConflict(conflictInfo, instMeta.version): #check if this version conflicts
                                hasConflicts = True
                                if meta.name in self.conflictPackages:
                                    self.conflictPackages[meta.name].append(conflictInfo.name)
                                else:
                                    self.conflictPackages[meta.name] = [conflictInfo.name]  
        db.close()
        return hasConflicts
    
    '''--------------------------------------------------------------------
    Check if conflict stands by checking against checkVersion
    depending on value of operator.
    Note: depInfo is a tuple (name,operator,version)
    --------------------------------------------------------------------''' 
    def __isConflict(self, conflictInfo, checkVersion):
        result = None
        if conflictInfo.version == None:
            result = True
        else:
            compareResult = self.CompareVersions(checkVersion, conflictInfo.version)
            if conflictInfo.operator == ">=":
                if compareResult != -1:
                    result = True
                else:
                    result = False                    
            elif conflictInfo.operator == "<=":
                if compareResult < 1:
                    result = True
                else:
                    result = False
            elif conflictInfo.operator == ">":
                if compareResult == 1:
                    result = True
                else:
                    result = False
            elif conflictInfo.operator == "<":
                if compareResult == -1:
                    result = True
                else:
                    result = False                    
        return result
 
         
    
    '''----------------------------------------------------------------
    Get the meta data for each package by parsing local package 
    ----------------------------------------------------------------'''
    def __getLocalPackageMeta(self):
        for packageName in self.packageNameList:
            #Is the file in the current directory or the packages path
            packagePath=""
            if os.path.exists(os.getcwd()+"/"+packageName):
                packagePath=os.getcwd()+"/"
            elif os.path.exists(self.conf["settings"]["packages_path"]+packageName):    
                packagePath=self.conf["settings"]["packages_path"]
            else:
                raise Exception(packageName +" could not be found")
            
            util.emptyDirectory(constants.WORKING_FOLDER)
            tar.ExtractFiles(packagePath+packageName, constants.WORKING_FOLDER, ".PKGINFO")
            if os.path.exists(constants.WORKING_FOLDER+".PKGINFO"):
                #open the PKGINFO
                pkgInfo=open(constants.WORKING_FOLDER+".PKGINFO","r")
                lines=pkgInfo.readlines()
                meta=MetaData()
                for line in lines:
                    if line.startswith("pkgname"):
                        meta.name=self.__getValue(line)
                    elif line.startswith("pkgver"):
                        meta.version=self.__getValue(line)
                    elif line.startswith("pkgdesc"):
                        meta.description=self.__getValue(line)
                    elif line.startswith("url"):
                        meta.url=self.__getValue(line)
                    elif line.startswith("size"):
                        meta.isize=self.__getValue(line)
                    elif line.startswith("pkgrel"):
                        meta.pkgRelease=self.__getValue(line)    
                    elif line.startswith("arch"):
                        meta.arch=self.__getValue(line)
                        if meta.arch!=self.conf["settings"]["architecture"] and meta.arch != "any":
                            raise Exception(meta.name+" ERROR: \n"+meta.arch+" is not supported on this system")
                    elif line.startswith("license"):
                        meta.license=[]
                        meta.license.append(self.__getValue(line))
                    elif line.startswith("depend"):
                        #print("GOT DEPENDS  **************************")
                        meta.depends.append(self.__getValue(line))
                        #print(meta.depends)
                    elif line.startswith("makedepend"):
                        meta.makedepends.append(self.__getValue(line))    
                    elif line.startswith("confict"):
                        meta.conflicts.append(self.__getValue(line))
                    elif line.startswith("provides"):
                        meta.provides.append(self.__getValue(line))
                    elif line.startswith("optdepends"):
                        meta.optionalDependencies.append(self.__getValue(line))
                    elif line.startswith("replaces"):
                        meta.replaces.append(self.__getValue(line))
 
                    
                if self.options.AUR:
                    meta.repository="AUR"                        
                else:
                    meta.repository="local"
                        
                meta.distro="Arch Linux"
                meta.packageFileName=packageName
                    
                if len(meta.optionalDependencies)==0:
                    meta.optionalDependencies.append("None")
                        
                if len(meta.depends)==0:
                    meta.depends.append("None")
                        
                if len(meta.conflicts)==0:
                    meta.conflicts.append("None")
                        
                if len(meta.replaces)==0:
                    meta.replaces.append("None")
                        
                if len(meta.license)==0:
                    meta.license.append("None")   
                        
                if len(meta.requiredBy)==0:
                    meta.requiredBy.append("None")            
                        
                                                                      
            else:
                raise Exception("Could not get .PKGINFO for "+packageName)                    
        self.packageMetaList.append(meta)
    
    
    #helper function for reading .PKGINFO
    def __getValue(self,lineIn):
        linesplit=lineIn.split("=")
        if len(linesplit)>1:
            return linesplit[1].strip()
        else:
            return None
    
            
    '''----------------------------------------------------------------
    Get the meta data for each package.
    Note: At this point a check to see if packages are available
    should already have taken place.
    ----------------------------------------------------------------'''
    def __getPackageMeta(self):
        db = mpacdb(self.conf["settings"]["mpac_database"])
        for packageName in self.packageNameList:
            #log("Getting meta data for "+packageName) 
            row = db.getPackageFromSyncTable(packageName)
            if(row == None):
                #raise MpacExceptions.PackageNotInRepo(packageName)
                raise Exception(packageName+" is not in repositories")
            self.packageMetaList.append(self.DbSyncResultToMetaData(row))
        db.close()
    
    '''-----------------------------------------------------------
    Download the packages contained in self.packageMetaList
    -----------------------------------------------------------'''
    def DownloadPackages(self):
                
        result = True
        for packageMeta in self.packageMetaList:            
            if packageMeta.repository != "local" and packageMeta.repository!="AUR": #ignore local and AURpackages
                if download.GetFile(packageMeta.packageFileName, self.conf['Arch Repositories'][packageMeta.repository], self.conf['settings']['packages_path'])==-1:
                    result = 'Could not download %s' % packageMeta.packageFileName 
                    break
        return result
    
    '''--------------------------------------------------------------
    make sure the local package  exists in the mpac packages path
    --------------------------------------------------------------'''
    def CopyLocalPackages(self):
        currentDir=os.getcwd()+"/"
        for packageMeta in self.packageMetaList:
            if (os.path.exists(currentDir+packageMeta.packageFileName)) and (os.getcwd()+"/" != self.conf["settings"]["packages_path"]):
                #the file is not in the packages path so copy it now
                shutil.copyfile(currentDir+packageMeta.packageFileName, self.conf["settings"]["packages_path"]+"/"+packageMeta.packageFileName)
        
            
    
    #must install dependencies first
    def setInstallOrder(self):
        #add any new packagea to the top on packageMetaList then delete newPackages list
        self.packageMetaList.extend(self.newPackages)        
        #del(self.newPackages)
        #self.packageMetaList.extend(self.newPackages)
        self.packageMetaList.reverse()
        self.newPackages=[]
        
    '''
    PROBLEM: TARFILE MODULE DOES NOT SUPPORT XZ FORMAT UNTIL PYTHON 3.3 IS RELEASED - DUE AUG 2012!!!!!!!!!
    In the meantime I can use the subprocess module e.g.:
        x = subprocess.check_output("tar -tf /home/marc/mpac/pkg/firefox-9.0.1-1-x86_64.pkg.tar.xz",shell=True,universal_newlines=True)
        print(x)
    '''   
    #install packages from repo's -I <package1,package2>
    def CheckFileConflicts(self,packagesMarkedForRemove):        
        hasConflicts = False
        for meta in self.packageMetaList[:]:
            db = mpacdb(self.conf["settings"]["mpac_database"])
            #get a list of the files in the package file
            #log(self.conf["settings"]["packages_path"] + meta.packageFileName)#*************debug only *******
            #tar=tarfile.open(self.conf["settings"]["packages_path"]+meta.packageFileName,'r:xz')
            tarList = tar.ListFiles(self.conf["settings"]["packages_path"] + meta.packageFileName)
            #for tarInfo in tar:
            for fileName in tarList:
                fileName=constants.ROOT+fileName
                #print("FILENAME IS: "+fileName) #******DEBUG ONLY *******
                if os.path.isfile(fileName):
                    #file exists on the system (potential conflict)                    
                    owner = db.getFileOwner(fileName)              
                    #print(owner+ " owns "+fileName) # * DEBUG ONLY *********      
                    if owner == None:
                        if not self.options.force: #for --force opton we will allow the file with no owner to be overwrittrn
                            self.fileConflictInfo.append("No owner found for " + fileName + " you must use -f option to overwrite this file")
                            hasConflicts = True
                    elif owner in packagesMarkedForRemove:
                        pass # no conflict is offending package is due to be removed
                    elif owner != meta.name:
                        #file is owned by a different package
                        #is the package in the install list
                        pIndex = self.IsNameInMetaList(self.packageMetaList, owner)
                        if pIndex > 0:
                            #owner is in the install list so we need to see if this newer package still contains the file
                            #get a list of files from the downloaded package
                            #newTar=tarfile.open(self.conf["settings"]["packages_path"]+self.packageMetaList[pIndex].packageFileName,'r:xz')
                            newTarList = tar.ListFiles(self.conf["settings"]["packages_path"] + self.packageMetaList[pIndex].packageFileName)
                            for newFileName in newTarList:
                                newFileName=constants.ROOT+newFileName
                                if os.path.isfile(newFileName):
                                    #check for match
                                    if newFileName == fileName: 
                                        hasConflicts = True #conflict found
                                        self.fileConflictInfo.append(meta.packageFileName + " and " + self.packageMetaList[pIndex].name + " both contain file " + fileName)
                                        break
                        else:
                            #owner of the file is not a package in the install list or in remove list 
                            #so we have a file conflict                            
                            self.fileConflictInfo.append(meta.name + " want to write " + fileName + "but it is owned by " + owner)
                            hasConflicts = True
            db.close()
        return hasConflicts
                            
                                        
    def __CheckPreInstallScript(self,meta):            
        #process the install script
        attrib=pflags()
        script=InstallScript()
        script.LoadScript(meta.installScript)
        scriptResult=True
        if meta.isUpgrade:
            if script.hasPreUpgrade:
                scriptResult=script.RunPreUpgrade()
        else:
            if script.hasPreInstall:
                scriptResult=script.RunPreInstall()
        if not scriptResult:        
            #log warning
            printTerm("red","Warning: there was an error running the pre-install script for "+meta.name,attrib.bold)
             
    def __CheckPostInstallScript(self,meta):            
        #process the install script
        #print("checking post install script") ##debug only
        attrib=pflags()
        script=InstallScript()
        script.LoadScript(meta.installScript)
        scriptResult=True
        if meta.isUpgrade:
            if script.hasPostUpgrade:
                scriptResult=script.RunPostUpgrade()
        else:
            if script.hasPostInstall:
                scriptResult=script.RunPostInstall()
        if not scriptResult:        
            #log warning
            printTerm("red","Warning: there was an error running the post-install script for "+meta.name,attrib.bold)
    
    def InstallPackage(self,meta):        
        #for meta in self.packageMetaList:
        #    print(meta.name)
        #return True
        #for meta in self.packageMetaList:                    
        #first build the optional deps list so it can be referenced later
        if meta.optionalDependencies[0] != "None":
            self.optionalDepsDict[meta.name] = meta.optionalDependencies[:]
                
        #make sure the working directory is empty
        util.emptyDirectory(constants.WORKING_FOLDER)
                
        #copy the package file to the working directory 
        shutil.copy(self.conf["settings"]["packages_path"] + meta.packageFileName, constants.WORKING_FOLDER + meta.packageFileName)   
            
        #Get the config files (/etc/* and .INSTALL)
        if not tar.GetConfigFiles(constants.WORKING_FOLDER + meta.packageFileName, constants.WORKING_FOLDER):
            return "Error extracting config files from "+meta.packageFileName
            
        #delete the package file
        #os.remove(constants.WORKING_FOLDER+meta.packageFileName)
            
        #now we can check to see if we have a .INSTALL file
        if os.path.isfile(constants.WORKING_FOLDER+".INSTALL"):
            #print("Found .INSTALL file") #********* *DEBUG ONLY *******
            #now we can load the script file
            scriptFile=open(constants.WORKING_FOLDER+".INSTALL",'r')
            script=scriptFile.read()
            meta.installScript=script
            scriptFile.close()
        else:
            meta.installScript="No"
                
        if meta.installScript!="No":
            self.__CheckPreInstallScript(meta)
            
        #now lets process the config files (if any)
        #not this will also update config files for db along with hash value ready to update later
        #the db with later (self.configFilesForDB)
        self.__processConfigFiles(meta)
            
        #now we are ready to install all the files which is a simple recursive copy
        #print ("Copying files............") #DEBUG ONLY **********
        #distutils.dir_util.copy_tree(src, dst[, preserve_mode=1, preserve_times=1, preserve_symlinks=0, update=0, verbose=0, dry_run=0])
        #dir_util.copy_tree(constants.WORKING_FOLDER, constants.ROOT, 1, 0, 1, 1, 1, 0)
        #above method does not overwrite existing files to not using it!
           
        #remove meta data files
        if os.path.isfile(constants.WORKING_FOLDER+".INSTALL"):
            os.remove(constants.WORKING_FOLDER+".INSTALL")
        if os.path.isfile(constants.WORKING_FOLDER+".PKGINFO"):
            os.remove(constants.WORKING_FOLDER+".PKGINFO")

        #util.CopyFromWorkingToRootFolder()
        if not tar.InstallPackageFiles(constants.WORKING_FOLDER + meta.packageFileName, constants.ROOT):
            return "Error Installing %s"%meta.packageFileName
            
        #clean up the working folder
        util.emptyDirectory(constants.WORKING_FOLDER)
        
        #check post install script
        if meta.installScript != "No":
            self.__CheckPostInstallScript(meta)
           
        #update the packages table
        dbResult=self.__AddPackageToDB(meta)
        if dbResult != True:
            return dbResult
            
        #now update the files table
        dbResult=self.__AddPackageFilesToDB(meta)
        if dbResult != True:
            return dbResult
            
            
        return True
        
        
        
    '''---------------------------------------------------------------------------------
    Check the working folder for all files in etc directory.
    Compares the config MD5 from installed version (if any), the original packages
    and from the new package. 
    ---------------------------------------------------------------------------------'''
    def __processConfigFiles(self,meta):        
        attrib=pflags()
        etcFileList=[]
        if os.path.isdir(constants.WORKING_FOLDER+"etc"):
            #n.b. os.listdir does not return path
            etcFileList=util.GetAllFilesFromDir(constants.WORKING_FOLDER+"etc")
        #if os.path.isdir(constants.WORKING_FOLDER+"opt"):
            #optFileList=util.GetAllFilesFromDir(constants.WORKING_FOLDER+"opt")        
        #etcFileList=etcFileList.append(optFileList)
        self.configFilesForDB=[]
        db = mpacdb(self.conf["settings"]["mpac_database"])    
        #print(etcFileList) #debug only
        for etcFile in etcFileList:
            installConfigFile=False
            newHash=util.GetMD5(etcFile)            
            if self.options.verbose:
                print(etcFile+" new hash >> "+newHash)            
            currentHash=None
            #etcFileName=etcFile[etcFile.rfind('/')+1:] #get the file name from the fullpath
            #need to only strip the working folder path
            rootEtcPathAndFile = constants.ROOT+etcFile[len(constants.WORKING_FOLDER):] 
            #print("rootEtcPathAndFile = "+rootEtcPathAndFile) #*DEBUG ONLY ************
            self.configFilesForDB.append([rootEtcPathAndFile,newHash]) #store for updating the db with later
            #print("stripped file name "+rootEtcPathAndFile)
            if os.path.isfile(rootEtcPathAndFile):
                currentHash=util.GetMD5(rootEtcPathAndFile) #hash currently in file system
                if self.options.verbose:
                    print(rootEtcPathAndFile+" current hash >> "+currentHash)
                if meta.isReinstall or meta.isUpgrade:
                    #get the original hash from the installed package.
                    #print("checking /etc/"+etcFileName)
                    #origHash=db.getInstalledFileHash(meta.name,"/etc/"+etcFileName) #debug only
                    #print("DB QUERY "+meta.name+"  "+rootEtcPathAndFile) #debug only
                    originalHash=db.getInstalledFileHash(meta.name,rootEtcPathAndFile)
                    if self.options.verbose:
                        print(rootEtcPathAndFile+" original hash >> "+originalHash)
                                            
                    #Logic stolen fornm the pacman code   
                    if originalHash != None:                                
                        if originalHash==currentHash==newHash:
                            '''
                            original = X, current = X, new = X 
                            All three versions of the file have identical contents, so overwriting is
                            not a problem. Overwrite the current version with the new version and do not
                            notify the user. (Although the file contents are the same, this overwrite will
                            update the filesystem's information regarding the file's installed, modified,
                            and accessed times, as well as ensure that any file permission changes are
                            applied.) 
                            '''
                            installConfigFile=True
                        elif originalHash==currentHash and currentHash != newHash:
                            '''
                            original = X, current = X, new = Y 
                            The current version's contents are identical to the original's, but the new
                            version is different. Since the user hasn't modified the current version and the
                            new version may contain improvements or bugfixes, overwrite the current version
                            with the new version and do not notify the user. This is the only auto-merging
                            of new changes that pacman is capable of performing.                         
                            '''
                            installConfigFile=True
                        elif originalHash==newHash and currentHash != newHash:
                            '''
                            original = X, current = Y, new = X 
                            The original package and the new package both contain exactly the same
                            version of the file, but the version currently in the filesystem has been
                            modified. Leave the current version in place and discard the new version without
                            notifying the user. 
                            '''
                            #delete the file from new package
                            installConfigFile=False
                        elif currentHash==newHash and originalHash != currentHash:
                            '''
                            original = X, current = Y, new = Y 
                            The new version is identical to the current version. Overwrite the current
                            version with the new version and do not notify the user. (Although the file
                            contents are the same, this overwrite will update the filesystem's information
                            regarding the file's installed, modified, and accessed times, as well as ensure
                            that any file permission changes are applied.) 
                            '''
                            installConfigFile=True
                        elif originalHash != currentHash and originalHash != newHash and currentHash != newHash:
                            '''
                            original = X, current = Y, new = Z 
                            All three versions are different, so leave the current version in place,
                            install the new version with a .pacnew extension and warn the user about the new
                            version. The user will be expected to manually merge any changes necessary from
                            the new version into the current version. 
                            '''
                            #rename file
                            if self.options.verbose:
                                printTerm("cyan","saving "+etcFile+" as "+etcFile+".mpacnew",attrib.bold)
                            os.rename(etcFile, etcFile+".mpacnew")                                        
                        else:     
                            printTerm("red","Could not decide what to do with file so will use mpacnew extension for "+constants.WORKING_FOLDER+"etc/"+etcFile,attrib.bold)
                            printTerm("red","THIS IS PROBABLY A BUG!!!!!!!!!",attrib.bold)
                            os.rename(etcFile, etcFile+".mpacnew")
                    else:
                        printTerm("red","Warning could not get original hash for "+rootEtcPathAndFile,attrib.bold)
                        if UserInput.GetYesNoChoice("Do you want to replace the existing file? ") =='y':
                            installConfigFile=True
                        else:
                            installConfigFile=False    
          
                else:
                    #not upgrade or reinstall but the file does exist on the system.
                    # we have already checked file conflicts earlier so the user must have deleted this file
                    #so install the config file
                    installConfigFile=True
                    if self.options.verbose:
                        print(etcFile + " is found on system but we do not know who owns it as we are not upgrading or reinstalling")
            
            else:
                #file does not exist in file system so OK to install it
                installConfigFile=True
                if self.options.verbose:
                    print(etcFile + " is not found on system, so will be installed")

            if installConfigFile==False:
                if self.options.verbose:
                    print(etcFile + " will not be installed")
                if os.path.isfile(etcFile):
                    os.remove(etcFile)
            else:
                if self.options.verbose:
                    print(etcFile + " will be installed")
                #no need to take any action if config file is to be installed as it will get overwritten when installing package
                
        db.close()        
              
    
    def __AddPackageToDB(self,meta):
        db=mpacdb(self.conf["settings"]["mpac_database"])
        #remove if already installed
        if meta.isUpgrade or meta.isReinstall:
            dbResult=db.removePackage(meta.name)
            if dbResult != True:
                db.close()
                return dbResult            
        
        #update install date
        installDate = datetime.date.today()
        meta.installDate=installDate
        
        dbResult=db.addPackage(meta)
        if dbResult != True:
            db.close()            
            return (dbResult)    
        db.close()
            
        return True
  
    
    
    def __AddPackageFilesToDB(self,meta):
        
        db=mpacdb(self.conf["settings"]["mpac_database"])
        #get list of the new package files
        newFileList=[]
        tarList = tar.ListFiles(self.conf["settings"]["packages_path"] + meta.packageFileName)
        for fileName in tarList:
            newFileList.append(constants.ROOT+fileName)

        if meta.isUpgrade:                        
            #get a list of the currently installed files
            prevFileList=db.getPackageFiles(meta.name)            
            self.__RemoveOrphanFiles(prevFileList, newFileList)
            
        #remove old package files
        if meta.isUpgrade or meta.isReinstall:
            dbResult=db.removePackageFiles(meta.name)
            if dbResult != True:
                db.close()
                return dbResult                
            
        #add the new package files    
        dbResult=db.addPackageFiles(meta.name,newFileList)
        if dbResult != True:
            db.close()
            return dbResult
        
        #update the file hashes for config files
        if len(self.configFilesForDB)>0:
            dbResult=db.addPackageFileHashes(meta.name,self.configFilesForDB)
            if dbResult != True:
                db.close()
                return dbResult
        
        db.close()
        return True
  
    
    
    def __RemoveOrphanFiles(self,prevFileList,newFileList):
        for prevFile in prevFileList:
            if prevFile[fileTableCols.FILE] not in newFileList:
                #print(prevFile)
                if os.path.exists(prevFile[fileTableCols.FILE]):
                    os.remove(prevFile[fileTableCols.FILE])
         
    
    def UpdateRequiredBy(self):
        db=mpacdb(self.conf["settings"]["mpac_database"])
        for meta in self.packageMetaList:
            if db.checkPackageExists(meta.name):
                rows=db.getPackagesThatDependOn(meta.name)
                reqByField=""
                if rows != None:
                    for row in rows:
                        reqByField+=row[packageTableCols.NAME]+" "
                    db.updateRequiredByFields(meta.name,reqByField)
        
        
