'''
Created on 25 Oct 2011

FOR Testing functions********************

@author: marc
'''

from ConfParser import ConfigParser
import constants
#from database import mpacdb
#import UserInput
#from PackageInstaller import PackageInstaller
from PackageBase import PackageBase
import http.client
import urllib.request
import sys
import string
from MetaData import MetaData
from database import mpacdb
import TermFunc

#import curses



def bylength(word1, word2):
    return len(word1) - len(word2)

def test():
    '''
    optDeps="gedit: text blah blah wank: piss flap poo another: what the fuck"
    list1=GetOptDepsListFromString(optDeps)
    x=0
    for l in list1:
        x=x+1
        print("%i %s"%(x,l))
    '''
 
    TermFunc.setColumn(5)
 
def GetOptDepsListFromString(optDeps):
    if optDeps=='None':
        return ['None']
        
    words = optDeps.split(" ")
    line=""
    depList=[]
    firstPass=True
    
    for word in words:
        word=word.strip()
        print("word = %s"%word)
        if(firstPass==False):
            if(word.endswith(":")):
                print("found :")
                depList.append(line)
                line=""        
        else:
            firstPass=False
        
        line+=word+" "
            
    depList.append(line)   
    return depList     
    
def CompareVersions(versionString1, versionString2):
        
        #format the version strings
        
        versionString1=__FormatVersionString(versionString1)
        versionString2=__FormatVersionString(versionString2)
        
        #now split the decimal points to equalise string
        #will pad left '0' 
        version1Split=versionString1.split(".")
        version2Split=versionString2.split(".")
        
        
        listLength=len(version1Split) #initially assume equal number of elements
        if len(version1Split)>len(version2Split):
            listLength=len(version2Split)
        elif len(version2Split)>len(version1Split):    
            listLength=len(version1Split)
            
        
        #compare lengths of the elements and pad left with 0's
        for i in range(listLength):
            if len(version1Split[i]) > len(version2Split[i]):
                version2Split[i] = version2Split[i].zfill(len(version1Split[i]))
            elif len(version2Split[i]) > len(version1Split[i]):
                version1Split[i] = version2Split[i].zfill(len(version2Split[i]))
        
        #now rebuild
        versionString1=""
        versionString2=""
        for partNum in version1Split:
            versionString1+=partNum
                    
        for partNum in version2Split:
            versionString2+=partNum
        
        
        print("VERSION 1 STRING "+versionString1) #debug
        print("VERSION 2 STRING "+versionString2) #debug
        if versionString1<versionString2:
            print ("version1 is less than version2") #debug
            return -1
        elif (versionString1>versionString2):
            print ("version1 is greater than version2") #debug
            return 1
        else:
            print ("version1 is equal to version2") #debug
            return 0 #equal    


def __FormatVersionString(versionString):
        versionString=versionString.strip()        
        #replace - with decimal point
        versionString=versionString.replace("-",".")
        versionString=versionString.lower()
        
        #remove illegal characters, replace with spaces
        versionString=versionString.translate(str.maketrans("abcdefghijklmnopqrstuvwxyz-_*%!^","                                "))
        
        #remove spaces
        versionString=versionString.replace(" ","")
        
        
        
        return  versionString
    
    
#THIS CAN BE IMPROVED!!!
def download(fileName,url,downloadPath):

    splitUrl=url.split("/")
    site=splitUrl[0]
    urlPath="/"
    for s in splitUrl[1:]:
        urlPath+=s+"/"
        
    urlPath=urlPath[:-1] #remove last "/"

    #print("URL PATH "+urlPath) #debug

    conn=http.client.HTTPConnection(site)
    conn.request("GET",urlPath+fileName)
    res=conn.getresponse() 
    file_size=res.length
    print("file size =",file_size)       
    print(res.status)
    print(res.reason) 
    f = open(downloadPath+fileName, 'wb')
    outMsg="Downloading "+fileName+"      >> "
    print (outMsg,end=" ")  #no new line        
    file_size_dl = 0
    block_sz = 4096
    #msgSize=len(outMsg)
    while True:
        buffer = res.read(block_sz)
        if not buffer:
            break

        file_size_dl += len(buffer)
        f.write(buffer)
         
        progress=file_size_dl * 100 / file_size
        
        #print("\b"*msgSize,end="") # backspace over last message
        print("\b\b\b\b",end="") # backspace over progress
        print("%3i" % progress,end="%")
        sys.stdout.flush()
    print()
    f.close()            
    if (file_size != file_size_dl) or (res.status != 200):
        print("Download error: "+res.reason)
        return False

    print("Download sucessful")
    return True

    



test()













