'''
Created on 6 Jan 2012

@author: marc
'''

import os
import constants
import sys
import subprocess

class InstallScript(object):

    def __init__(self):
        self.hasPreInstall=False
        self.hasPostInstall=False
        self.hasPreRemove=False
        self.hasPostRemove=False
        self.hasPreUpgrade=False
        self.hasPostUpgrade=False
        self.ScriptText="";
        
    
    def __runScript(self,call_method):
        try:           
            if os.path.isfile(constants.ROOT+"mpacInstallScript.sh"):
                os.remove(constants.ROOT+"mpacInstallScript.sh")
            #write the script file            
            scr = open(constants.ROOT+"mpacInstallScript.sh","w")
            scr.write(self.ScriptText+"\n"+call_method)          
            scr.close()
            #make the script executable by owner n.b 0o indictaes octal            
            os.chmod(constants.ROOT+"mpacInstallScript.sh",0o744)
            #run the script
            os.chdir(constants.ROOT)
            subprocess.call([constants.SHELL,constants.ROOT+"mpacInstallScript.sh"],shell=False)
        except:
            print("Error running "+ call_method+ "+script") #**** DEBUG ONLY ********
            print(sys.exc_info())
            return False
        return True    
    
        
    def LoadScript(self,scriptIn):
        self.ScriptText=scriptIn
        if len(self.ScriptText)>0:
            if self.ScriptText.find("pre_install") != -1:
                self.hasPreInstall=True
            if self.ScriptText.find("post_install") != -1:
                #print("Found post_install script") #debug on;y ***********
                self.hasPostInstall=True
            if self.ScriptText.find("pre_upgrade") != -1:
                self.hasPreUpgrade=True
            if self.ScriptText.find("post_upgrade") != -1:
                self.hasPostUpgrade=True
            if self.ScriptText.find("pre_remove") != -1:
                self.hasPreRemove=True
            if self.ScriptText.find("post_remove") != -1:
                self.hasPostRemove=True
                                
    def RunPreInstall(self):
        if self.__runScript("pre_install"):
            return True
        else:
            return False
                      
    def RunPostInstall(self):
        if self.__runScript("post_install"):
            return True
        else:
            return False

    def RunPreUpgrade(self):
        if self.__runScript("pre_upgrade"):
            return True
        else:
            return False

    def RunPostUpgrade(self):
        if self.__runScript("post_upgrade"):
            return True
        else:
            return False

    def RunPreRemove(self):
        if self.__runScript("pre_remove"):
            return True
        else:
            return False

    def RunPostRemove(self):
        if self.__runScript("post_remove"):
            return True
        else:
            return False
                 
    