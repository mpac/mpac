'''
Created on Mar 6, 2012

@author: marc
'''
from AURInstaller import AURInstaller
from DependencyResolver import DependencyResolver
import InstallTerminal
import glob
import os
import shutil
from TermFunc import printTerm
from TermFunc import pflags

def doAUR(conf,packageList,options):
    '''
    How this could work.....
    1) Parse each package and create meta data list.
    
    2) check make depends of each package, if makedepends not installed and not in packageList then return error message.
       instructing user to install the make depends first. If the makedepends is in the package list then make sure
       this would be built first and installed first (change list order)
    
    3) Resolve dependnecies. If any unresolved dependencies found check the AUR for these dependnecies, if they
       exist in the AUR add to package list and repeat step 2. If dependnecies are not found then return error.   
    
    4) Make each package and move built package to the packages directory
    
    5) Call install local package to actually install the packages.    
    '''
    attrib=pflags()
    aur = AURInstaller(conf,options)    
    aur.GetPackages(packageList)
    Continue=True
    if len(aur.ErrorMessage)>0:
        Continue=False
        for message in aur.ErrorMessage:
            printTerm(attrib.COL_ERR_WARN,message,attrib.bold)
    
    if Continue:
        #unpack the archive package builds and parse packages - also checks package is not already installed
        if aur.ParsePackages(packageList)==False:
            Continue=False
            if len(aur.ErrorMessage)>0:                
                for message in aur.ErrorMessage:
                    printTerm(attrib.COL_ERR_WARN,message,attrib.bold)
                    
    depResolver = DependencyResolver(conf,options)    
    if Continue:
        #check make dependencies        
        for meta in aur.packageMetaList:
            printTerm(attrib.COL_INFO,">> Checking make depends for %s..."%meta.name,attrib.bold)
            for makedepend in meta.makedepends:
                if depResolver.CheckInstalledPackages(makedepend)==False:
                    if depResolver.CheckRepoPackages(makedepend, meta.name)==False:
                        #make depends not found - could in future check AUR but won't do for now!
                        printTerm(attrib.COL_ERR_WARN,"make depend "+makedepend+ "for "+meta.name+" is not installed",attrib.bold)
                        Continue=False
                 
    #do we need to install extra make depends?
    if Continue:    
        if len(depResolver.ExtraPackages)>0:
            #there are extra packages to install for make depends
            printTerm(attrib.COL_INFO,">> Extra packages for make depends will be installed...",attrib.bold)
            for meta in depResolver.ExtraPackages:
                printTerm(attrib.COL_MAIN_BODY,"  "+meta.name+"-"+meta.version,attrib.bold)
            print("")
            
    #now check dependencies        
    '''
    ** Not necessary as dependencies get checked from parsing local package later and this
    ** is more reliable
    e.g AUR Linux tycoon PKGBUILD:
        if [[ $CARCH = 'i686' ]]; then
            depends=(gtk2)
        elif [[ $CARCH = 'x86_64' ]]; then
            depends=(lib32-gtk2)
        fi
    **    
    if Continue:
        for meta in aur.packageMetaList:
            printTerm(attrib.COL_INFO,">> Checking dependencies for %s..."%meta.name,attrib.bold)
            for depend in meta.depends:
                if depResolver.CheckInstalledPackages(depend)==False:
                    if depResolver.CheckRepoPackages(depend, meta.name)==False:
                        #make depends not found - could in future check AUR but won't do for now!
                        printTerm(attrib.COL_ERR_WARN,"dependency "+depend+ " for "+meta.name+" is not installed",attrib.bold)
                        Continue=False        
    
    if Continue:
        #install any extra packages (as dependnecies) 
        if len(depResolver.ExtraPackages)>0:
            #yes we do
            depsList=[]
            for dep in depResolver.ExtraPackages:
                depsList.append(dep.name)
            depsOptions=options
            depsOptions.asdependency=True    
            Continue=InstallTerminal.DoInstall(conf, depsList, depsOptions)
    '''        
    if Continue:
        #printTerm(attrib.COL_MAIN_BODY,"Install package(s) from AUR....",attrib.bold)
        if aur.MakePackages():
            pass
        else:    
            for errorMsg in aur.ErrorMessage:                
                printTerm(attrib.COL_ERR_WARN,errorMsg,attrib.bold)
   
    if Continue:
        localInstallList=[]
        for packageToInstallMeta in aur.packagesMadeOK:
            #Unfortunately {meta}.packageFileName is not in the PKGBUILD so we
            #need to work out the built package name. I will assume the package will be .xz archive.
            #so get a list of [name]-[version]*.xz
            fileList = glob.glob(conf["settings"]["AUR_path"]+packageToInstallMeta.name+"/*pkg.tar.xz")
            if len(fileList)!=1:
                printTerm(attrib.COL_ERR_WARN,"Could not get built package name",attrib.bold)
            else:
                printTerm(attrib.COL_MAIN_BODY,"will install "+fileList[0],attrib.bold) 
                packageName=os.path.basename(fileList[0])
                #copy the built package to the package directory
                shutil.copy(fileList[0], conf["settings"]["packages_path"]+packageName)
                localInstallList.append(packageName)
            
        #now do a local package install
        if len(localInstallList)>0:
            options.local=True # force local install
            InstallTerminal.DoInstall(conf,localInstallList,options)
                
                
                  
