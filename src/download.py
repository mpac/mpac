'''
Created on 24 Nov 2011

@author: marc
'''
import http.client
import os.path
import sys
from TermFunc import printTerm
from TermFunc import pflags
import time

#THIS CAN BE IMPROVED!!!
def GetFile(fileName,url,downloadPath):

    attrib=pflags()
    
    #Return values:  0= downloaded Ok, 1=already up to date on system, -1=error
    
    splitUrl=url.split("/")
    site=splitUrl[0]
    urlPath="/"
    for s in splitUrl[1:]:
        urlPath+=s+"/"
        
    urlPath=urlPath[:-1] #remove last "/"

    #print("URL PATH "+urlPath+fileName) #debug

    conn=http.client.HTTPConnection(site)
    conn.request("GET",urlPath+fileName)
    res=conn.getresponse() 
    
    
    remoteFileLength = res.length # gets file length
    
    #Note could also use res.info() - also contains date/time as well as length
        
    #now check if the file exists
    if os.path.isfile(downloadPath+fileName):
        if os.path.getsize(downloadPath+fileName)==remoteFileLength: #check file size incase previous download did not complete.
            printTerm(attrib.COL_MAIN_BODY,'%s is up to already up to date' % fileName,attrib.bold)
            return 1
        
    
    #print(info[0]) #printing for debug only
    
    file_size=res.length
    #print("file size =",file_size)       
    #print(res.status)
    #print(res.reason) 
    f = open(downloadPath+fileName, 'wb')
    outMsg=">> Downloading "+fileName
    printTerm(attrib.COL_MAIN_BODY,outMsg,attrib.bold|attrib.noNewLine)    
    #print (outMsg,end=" ")  #no new line 
    
    #format file_size for printing
    check_size=file_size/1024 # bytes to KB'
    if check_size <= 1024:
        fileSizeString="%.2fKB"%check_size
    else:
        fileSizeString="%.2fMB"%(check_size/1024)
    
    file_size_dl = 0
    block_sz = 4096
    #msgSize=len(outMsg)
    while True:
        start_time = int(time.time())
        buffer = res.read(block_sz)
        stop_time = int(time.time())
        if not buffer:
            break

        file_size_dl += len(buffer)
        f.write(buffer)
         
        progress=file_size_dl * 100 / file_size
        
        #convert bytes to KB
        downloaded=file_size_dl/1024
        
        #printTerm("magenta", "%3i" % progress+'%', attrib.rightMargin|attrib.bold|attrib.noNewLine)
        
        if file_size_dl <= 1024:
            # display in KB
            if (stop_time-start_time >= 0.5) or (file_size_dl == file_size):
                printTerm(attrib.COL_HEADER, "[%.2fKB of %s] %3i" % (downloaded,fileSizeString,progress)+"%", attrib.rightMargin|attrib.bold|attrib.noNewLine)
        else:
            #display in MB    
            if (stop_time-start_time >= 0.5) or (file_size_dl == file_size):
                printTerm(attrib.COL_HEADER, "[%.2fMB of %s] %3i" % (downloaded/1024,fileSizeString,progress)+"%", attrib.rightMargin|attrib.bold|attrib.noNewLine)
        sys.stdout.flush()
    
    f.close()            
    if (file_size != file_size_dl) or (res.status != 200):
        printTerm(attrib.COL_ERR_WARN,"\nDownload error: "+res.reason,attrib.bold)
        return -1
    print()
    return 0