'''
Created on 11 Oct 2011

@author: marc
'''
#! /usr/bin/env python
import sqlite3
import util
import packageTableCols
import SyncTableCols

class mpacdb():
    def __init__(self,pathToDB):
        self.pathToDB=pathToDB
        self.cursor=0
        self.conn=None
        self.__connect()        
         
        
    def close(self):
        self.cursor.close()    
            
    def __connect(self):
        self.conn = sqlite3.connect(self.pathToDB) #@UndefinedVariable
        self.conn.text_factory = str
        self.cursor = self.conn.cursor()
        
    def commit(self):
        self.conn.commit()

    
    def searchInstalled(self,name,includeDesc):
        if includeDesc:
            self.cursor.execute("select name,version,description from packages where name like '%"+name+"%' or description like '%"+name+"%'")
            rows = self.cursor.fetchall()
        else:
            self.cursor.execute("select name,version,description from packages where name like '%"+name+"%'")
            rows = self.cursor.fetchall()
        if rows != None:
            return rows
    
    def searchRepos(self,name,includeDesc):
        if includeDesc:
            self.cursor.execute("select name,version,description from sync where name like '%"+name+"%' or description like '%"+name+"%'")
            rows = self.cursor.fetchall()
        else:
            self.cursor.execute("select name,version,description from sync where name like '%"+name+"%'")
            rows = self.cursor.fetchall()
        if rows != None: 
            return rows    
            
    def getGroupPackageNames(self,group):
        self.cursor.execute("select * from sync where packagegroup like '%"+group+"%'")
        rows = self.cursor.fetchall()
        #print (rows)
        if rows != None:
            rows=self.checkResults(group, SyncTableCols.PACKAGE_GROUP,rows)
        return rows          
    
    def getInstalledGroupPackageNames(self,group):
        self.cursor.execute("select * from packages where packagegroup like '%"+group+"%'")
        rows = self.cursor.fetchall()
        #print (rows)
        if rows != None:
            rows=self.checkResults(group, SyncTableCols.PACKAGE_GROUP,rows)
        return rows             

    def checkPackageInSyncTable(self,name):
        self.cursor.execute("select name from sync where name = ?",[name])
        data=self.cursor.fetchone()
        if data == None:
            return False
        else: 
            return True
    
    
    def removeFromSyncTable(self,name,repo):
        self.cursor.execute("delete from sync where name = ? and reponame = ?",[name,repo])
        self.conn.commit()       
        if self.cursor.rowcount != 1:
            return ("Database error: Could not remove from sync table "+name)        
        return True
    
    #note: this function does not commit changes
    def updateSyncPackage(self,meta):        
        values=(meta.name,meta.version,meta.description,util.GetStringsFromList(meta.groups),\
               util.GetStringsFromList(meta.depends),util.GetStringsFromList(meta.optionalDependencies),\
               util.GetStringsFromList(meta.conflicts),util.GetStringsFromList(meta.replaces),util.GetStringsFromList(meta.provides),\
               meta.url,meta.isize,meta.csize,meta.arch,util.GetStringsFromList(meta.license),meta.distro,meta.repository,meta.packageFileName,meta.name)
        
        query="update sync set "
        query+="name = ?"
        query+=",version = ?"
        query+=",description = ?" 
        query+=",packagegroup = ?"
        query+=",depends = ?"
        query+=",optionaldeps = ?"
        query+=",conflictswith = ?"
        query+=",replaces = ?"
        query+=",provides = ?"
        query+=",url = ?"
        query+=",installedsize = ?"
        query+=",downloadsize = ?"
        query+=",architecture = ?"
        query+=",licenses = ?"
        query+=",distro = ?"
        query+=",reponame = ?"
        query+=",packagefilename = ?"        
        query+=" where name=?"
        #print("QUERY ------ "+query)
        self.cursor.execute(query,values)
        if self.cursor.rowcount != 1:            
            return ("Database error: Could not update sync table package "+meta.name+"\n"+query)
            #return False
        return True    
        
        
    #note: this function does not commit changes
    def addSyncPackage(self,meta):
        metaTuple=(meta.name,meta.version,meta.description,util.GetStringsFromList(meta.groups) \
            ,util.GetStringsFromList(meta.depends),util.GetStringsFromList(meta.optionalDependencies),\
            util.GetStringsFromList(meta.conflicts),\
            util.GetStringsFromList(meta.replaces),util.GetStringsFromList(meta.provides),meta.url,\
            meta.isize,meta.csize,meta.arch,util.GetStringsFromList(meta.license),\
            meta.distro,meta.repository,meta.packageFileName)    
        
        self.cursor.execute('insert into sync values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',metaTuple)
        if self.cursor.rowcount != 1:            
            return ("Database error: Could not add package to sync table "+meta.name)
            #return False
        return True
    
    
    def getAllPackagesFromRepo(self,repo):
        self.cursor.execute("select * from sync where reponame = ?",[repo])
        rows=self.cursor.fetchall()       
        return rows
                
    
    def updateRequiredByFields(self,name,reqByField):
        self.cursor.execute("update packages set requiredby = ? where name = ?",(reqByField,name))
        self.conn.commit()
        if self.cursor.rowcount < 1:            
            #log ("Database error: Could not update requiredby fields for: "+name)
            return False
            
        return True        
    
    
    def getPackagesThatDependOn(self,name):
        self.cursor.execute("select * from packages where depends like '%"+name+"%'")
        rows = self.cursor.fetchall()
        if rows != None:
            rows=self.checkResults(name, packageTableCols.DEPENDS, rows)
        return rows
    

    def getPackagesThatOptDependOn(self,name):
        self.cursor.execute("select * from packages where optionaldeps like '%"+name+"%'")
        rows = self.cursor.fetchall()
        if rows != None:
            rows=self.checkResultsForOptDeps(name, packageTableCols.OPT_DEPENDS, rows)
        return rows    
  
    '''
    def getPackagesThatRequire(self,name):
        self.cursor.execute("select * from packages where name is not '" +name+"' and requiredby like '%"+name+"%'")
        rows = self.cursor.fetchall()
        if len(rows)==0:
            rows=None
        else:
            rows=self.checkResults(name, packageTableCols.REQUIRED_BY, rows)
        return rows
    '''
    
    def addPackageFileHashes(self,name,configFilesWithHashes):
        for configFile in configFilesWithHashes:
            self.cursor.execute("update files set hash = ? where file= ? and package = ?",(configFile[1],configFile[0],name))
        self.conn.commit()
        if self.cursor.rowcount < 1:            
            return ("Database error: Could not update file hash: "+name+ " "+configFile)
        return True        
    
    
    def addPackageFiles(self,name,fileList):
        for file in fileList:
            #print("add file to db: "+file)
            self.cursor.execute("insert into files values (?,?,?)",(file,name," "))
        self.conn.commit()
        if self.cursor.rowcount < 1:            
            return ("Database error: Could not add package files: "+name)
        return True        
    
    def removePackageFiles(self,name):
        self.cursor.execute("delete from files where package = ?",[name]);
        self.conn.commit()
        #if self.cursor.rowcount < 1:            
            #return ("Database error: Could not remove package files: "+name)
        return True     
    
    
    def addPackage(self,meta):                
        #try:
        #print("would add to db "+meta.name)
        metaTuple=(meta.name,meta.version,meta.description,util.GetStringsFromList(meta.groups) \
               ,util.GetStringsFromList(meta.depends),util.GetStringsFromList(meta.optionalDependencies),\
               util.GetStringsFromList(meta.requiredBy),util.GetStringsFromList(meta.conflicts),\
               util.GetStringsFromList(meta.replaces),util.GetStringsFromList(meta.provides),meta.url,\
               meta.installScript,meta.installReason,meta.installDate,meta.isize,meta.arch,util.GetStringsFromList(meta.license),\
               meta.distro,meta.repository)
 
            
        self.cursor.execute('insert into packages values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',metaTuple)
        #print("ROW COUNT IS %i"%self.cursor.rowcount) #******** DEBUG ONLY *************        
        self.conn.commit()
        if self.cursor.rowcount != 1:            
            return ("Database error: Could not add package "+meta.name)
            #return False
        return True
    
    
    def removePackage(self,name):
        #print("would delete from db "+name)
        self.cursor.execute("delete from packages where name = ?",[name])
        self.conn.commit()       
        if self.cursor.rowcount != 1:
            return ("Database error: Could not remove package "+name)        
        return True
   
    
    def getInstalledFileHash(self,packageName,file):
        self.cursor.execute("select hash from files where package = ? and file = ?",(packageName,file))
        data=self.cursor.fetchone()
        if data != None:
            return data[0]
        else: 
            return None
    
    def getFileOwner(self,file):
        self.cursor.execute("select package from files where file = ?",[file])
        data=self.cursor.fetchone()
        if data != None:
            return data[0]
        else: 
            return None
    
    
    def checkPackageIsInstalled(self,packageName):
        exists=True
        pname=(packageName,)
        self.cursor.execute("select name from packages where name = ?",pname)
        data = self.cursor.fetchone()
        if (not data):
            exists=False        
        return exists
    
    
    def checkPackageExists(self,packageName):
        exists=True
        pname=(packageName,)
        self.cursor.execute("select name from sync where name = ?",pname)
        data = self.cursor.fetchone()
        if (not data):
            exists=False        
        return exists

    
    def getPackageFromSyncTable(self,packageName):        
        pname=(packageName,)
        #print("getting ",packageName) #*****DEBUG ONLY *******
        self.cursor.execute("select * from sync where name = ?",pname)
        row = self.cursor.fetchone()
        return row
    
    def getInstalledPackage(self,packageName):
        pname=(packageName,)
        self.cursor.execute("select * from packages where name = ?",pname)
        row = self.cursor.fetchone()
        return row #note returns None if now rows match
    
    def getProviderInstalledPackageFor(self,packageName):
        #pname = (packageName,)        
        #****** THIS IS NOT WORKING BAD FORMAT *****************
        #self.cursor.execute("select name,provides from sync where provides like '%"+"?"+"%'",pname)
        self.cursor.execute("select name,provides from packages where provides like '%"+packageName+"%'")
        rows = self.cursor.fetchall()
        if rows != None:
            rows=self.checkResults(packageName, 1, rows)
        return rows
    
    def getProviderRepoPackageFor(self,packageName):
        #pname = (packageName,)        
        #****** THIS IS NOT WORKING BAD FORMAT *****************
        #self.cursor.execute("select name,provides from sync where provides like '%"+"?"+"%'",pname)
        self.cursor.execute("select name,provides from sync where provides like '%"+packageName+"%'")
        rows = self.cursor.fetchall()      
        return rows

    
    def getPackageVersion(self,packageName):        
        pname=(packageName,)
        self.cursor.execute('select version from packages where name = ?',pname)
        data = self.cursor.fetchone()
        return data[0]  
    
    #note returns a list of tuples (<file>,)
    def getPackageFiles(self,packageName):
        #print("META.NAME "+packageName)
        self.cursor.execute('select file from files where package = ?',[packageName])            
        flist = self.cursor.fetchall()
        #print(flist)
        if len(flist)==0:
            flist=None

        return flist
          
    def getPackagesNameAndVersionFromRepo(self,repository):
        self.repository = repository
        self.cursor.execute('select name,version from sync where reponame = ?',[repository])
        plist = self.cursor.fetchall()
        if len(plist)==0:
            plist=None

        return plist
    
    def getAllPackagesNameAndVersion(self):        
        self.cursor.execute('select name,version from packages')
        plist = self.cursor.fetchall()
        if len(plist)==0:
            plist=None
        return plist
    
    
    def GetAllPackagesInstalledAsDep(self):
        self.cursor.execute("select * from packages where installreason = 'Installed as a dependency for another package'")
        plist=self.cursor.fetchall()
        if len(plist)==0:
            plist=None
        return plist
    
    '''
    When using the like sql function to return results looking for packages that e.g. depend on pacman
    would return pacman-mirror so we need to correct the results.
    We need to find the results that really do match name and remove the ones that do not. There must
    be a more elegeant solution than this but until then....!!!!
    '''
    def checkResults(self,name,colToCheck,resultsToCheck):
        name = util.StripOperators(name)
        modCheckList=[]
        for dbResult in resultsToCheck:
            gotMatch=False
            checkList=dbResult[colToCheck].split()
            for listItem in checkList:
                if util.StripOperators(listItem)==name:
                    gotMatch=True
                    break;
            if gotMatch:
                #add to list result is valid   
                modCheckList.append(dbResult)
        
        if len(modCheckList)==0:
            modCheckList=None
        return modCheckList

    
    #special case for opt deps - split by : and use first element as nane to check
    def checkResultsForOptDeps(self,name,colToCheck,resultsToCheck):
        name = util.StripOperators(name)
        modCheckList=[]
        for dbResult in resultsToCheck:
            gotMatch=False
            checkList=util.GetOptDepsListFromString(dbResult[colToCheck])
            for listItem in checkList:
                optName=listItem.split(':')
                if optName[0]==name:
                    gotMatch=True
                    break;
            if gotMatch:
                #add to list result is valid   
                modCheckList.append(dbResult)
        
        if len(modCheckList)==0:
            modCheckList=None
        return modCheckList