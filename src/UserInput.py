'''
Created on 13 Oct 2011

CONSOLE USER INPUT FUNCTIONS

@author: marc
'''
from TermFunc import printTerm
from TermFunc import pflags


def GetYesNoChoice(message):
    userin=""
    while ( (userin.lower() != "y") and (userin.lower() != "n") ):            
        userin=input(message+ " [y/n]")
    return userin.lower()


'''
Get a numeric input from user within a given number range
'''
def GetNumberChoice(message,maxNumber):
    attrib=pflags()
    gotNumber=False
    gotError=False
    while(gotNumber==False):
        num=input(message)
        try:
            num=int(num)
        except ValueError:
            printTerm("red","ERORR: try again fuck wit....",attrib.bold)
            gotError=True
                         
        if(gotError==False):
            if((num<=maxNumber) and (num>0)):
                gotNumber=True
            else:
                printTerm ("red","ERROR: Maximum number is %s".format(maxNumber).attrib.bold)
                
    if(gotNumber==True):
        return num
    else:
        return -1
