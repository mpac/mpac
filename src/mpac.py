#! /usr/bin/env python
 
#import sys
from ConfParser import ConfigParser
import constants
import InstallTerminal
import RemoveTerminal
import UpgradeTerminal
import QueryTerminal
import SyncTerminal
from optparse import OptionParser
import AURTerminal
import purgeDB
from collections import namedtuple
from TermFunc import scrollToEnd
#import os

#this is the function to run from a terminal
def main():
    
    scrollToEnd()
    
    #try:
    confIn=ConfigParser(constants.CONFIG_FILE)
    #DisplayHelp()
    #log (confIn.config["settings"]["architecture"])
      
    #print("CURRENT DIRECTORY ",os.getcwd())
    
    parser=OptionParser()      
    
    #Install command and install specific options
    parser.add_option("--install","-I", action="store_true", dest="install",help="Install packages")
    parser.add_option("--asdependency","-d", action="store_true", dest="asdependency",default=False,help="Install as a dependency")
    parser.add_option("--local","-l",action="store_true",dest="local",default=False,help="Install local package")
    
    #Sync command - will also be excepted with install/upgrade command to sync db before installing/upgrading
    parser.add_option("--sync","-s","-S",action="store_true",dest="sync",default=False,help="Sync mpac database with repositories")
    
    #Upgrade command
    parser.add_option("--upgrade","-U", action="store_true", dest="upgrade",default=False,help="upgrade all packages to current versions")
    
    #Remove command and remove specific options
    parser.add_option("--remove","-R", action="store_true", dest="remove",help="Remove packages from system")
    parser.add_option("--clean-deps","-c", action="store_true", dest="clean_deps",default=False,help="Try to remove dependnencies as well")
    
    #general options
    parser.add_option("--verbose","-v", action="store_true", dest="verbose",help="Be verbose")
    parser.add_option("--force","-f", action="store_true", dest="force",default=False,help="force overwriting of files (use with care)")    
    
    #Add AUR option
    parser.add_option("--AUR", action="store_true", dest="AUR",default=False,help="Install From Arch User Repository")
    
    #Purge database option
    parser.add_option("--purge-db", action="store_true", dest="purgedb",default=False,help="Delete packages from database (use with care)")
        
    #Add Query options
    parser.add_option("--query","-Q",action="store_true",dest="query",default=False,help="Query Database")
    
    #only needed for --query
    parser.add_option("--installed","-i",action="store_true",dest="installed",default=False,help="Only include installed packages (for use with Query command)")
    parser.add_option("--description",action="store_true",dest="description",default=False,help="Include description in search (for use with Query command)")
    parser.add_option("--repository","-r",action="store_true",dest="repository",default=False,help="Only include data from repositories (for use with Query command)")
 
    #actually parse the options
    (options,args) = parser.parse_args()
        
    #now process them!
    ProcessOptions(confIn.config,args,options)   
     
      

 

#-------------------------------------
#Lets see what the user wants to do
#-------------------------------------
def ProcessOptions(conf,args,options):
    
    #packages to install/remove are in args
    if options.sync:
        SyncTerminal.DoSync(conf,options)
    
    if options.install:
        if options.upgrade or options.remove or options.AUR or options.purgedb or options.query:
            raise Exception("**Error: invalid command options**")
        else:
            InstallTerminal.DoInstall(conf, args,options)
    elif options.remove:
        if options.upgrade or options.install or options.AUR or options.purgedb or options.query:
            raise Exception("**Error: invalid command options**")
        else:
            RemoveTerminal.DoRemove(conf,args,options)
    elif options.upgrade:
        if options.remove or options.install or options.AUR or options.purgedb or options.query:
            raise Exception("**Error: invalid command options**")
        else:
            UpgradeTerminal.DoUpgrade(conf, options)
    elif options.AUR:
        if options.remove or options.install or options.upgrade or options.purgedb or options.query:
            raise Exception("**Error: invalid command options**")
        else:
            AURTerminal.doAUR(conf,args,options)
    elif options.purgedb:
        if options.remove or options.install or options.upgrade or options.sync or options.query:
            raise Exception("**Error: invalid command options**")
        else:
            purgeDB.DoPurge(conf,args,options) 
    elif options.query:
        if options.remove or options.install or options.upgrade or options.sync or options.purgedb:
            raise Exception("**Error: invalid command options**")            
        else:
            # First arg is the command, followed by search options etc
            if len(args)==0:
                raise Exception("**Error: invalid command options**")
            else:
                #command_args=None
                command=args[0]
                qOptions=namedtuple("qOptions","installed description repository")
                qOptions.installed=options.installed
                qOptions.description=options.description
                qOptions.repository=options.repository
                query_args=[]
                for arg in args[1:]: #first arg is commsnd so skip it
                    #print(arg)
                    if arg.startswith("-")==False: # skip the options (processed seperately)
                        query_args.append(arg)
                        
                    
            QueryTerminal.DoQuery(conf,command,qOptions,query_args)    
   
#sys.p.path.append('/home/marc/programming/mpac/src')
#print("*********** MPAC DEBUG VERSION ***************")
main()
