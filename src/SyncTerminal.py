'''
Created on Feb 2, 2012

@author: marc
'''
import download
import util
from Syncer import Syncer
from TermFunc import printTerm
from TermFunc import pflags

def DoSync(conf,options):
    
    syncer=Syncer(conf,options)
    attrib=pflags()
    
    if options.force: #force download be deleting syn contents
        util.emptyDirectory( conf["settings"]["download_sync_path"])
    
    #get the repositories
    reposToSync=[] #add the ones we want to sync here
    print()
    for repo in conf["Arch Repositories"]:
        dl=download.GetFile(repo+".db.tar.gz", conf["Arch Repositories"][repo], conf["settings"]["download_sync_path"])
        if(dl==0):
            reposToSync.append(repo)
            
    if reposToSync:        
        printTerm(attrib.COL_INFO, ">> Gathering Data....", attrib.bold|attrib.noNewLine)
        syncer.GetRepositories(reposToSync)
        printTerm(attrib.COL_OK, "completed", attrib.rightMargin|attrib.bold)
        for repo in reposToSync:
            printTerm(attrib.COL_MAIN_BODY, ">> Updating %s...."%repo, attrib.bold|attrib.noNewLine)            
            syncer.UpdateDatabase(repo)
            printTerm(attrib.COL_OK, "completed", attrib.rightMargin|attrib.bold)
            if options.verbose:
                for vMessage in syncer.verboseMessages:
                    printTerm(attrib.COL_VERBOSE, " "+vMessage, attrib.bold)
        
        if syncer.normalMessages:
            for nMessage in syncer.normalMessages:
                printTerm(attrib.COL_OK, " "+nMessage, attrib.bold)
        
        if syncer.errorMessages:
            for eMessage in syncer.errorMessages:
                printTerm(attrib.COL_ERR_WARN, eMessage,attrib.bold)
            
        printTerm(attrib.COL_MAIN_BODY, "Sync process completed OK", attrib.bold)
    else:
        printTerm(attrib.COL_MAIN_BODY, "Nothing to sync", attrib.bold)
        
