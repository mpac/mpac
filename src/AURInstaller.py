'''
Created on Mar 8, 2012

@author: marc
'''
from PackageBase import PackageBase
import tar
import download
import constants
import os
from MetaData import MetaData
import subprocess


#Must remember to break the functions down so calling function can go step by step
class AURInstaller(PackageBase):
    
    def __init__(self, conf,options):
        #self.packageNameList = [] # a list of package names fron the user
        self.packageMetaList = [] # all meta data for package list
        self.conf=conf
        self.options=options
        self.ErrorMessage=[]
        self.packagesMadeOK=[]
        PackageBase.__init__(self, conf)  #super(PackageBase, self,conf).__init__()


    
    def GetPackages(self,packageNameList):
        for packageName in packageNameList:
            result=download.GetFile('/'+packageName+".tar.gz", constants.AUR_URL+packageName, self.conf["settings"]["AUR_path"])
            if result==-1:
                self.ErrorMessage.append("Could not find "+packageName+" in AUR")    
    
    '''
    Parse the AUR package list and populate meta data structure
    '''
    def ParsePackages(self,packageNameList):
        self.ErrorMessage=[] #clear any previous errors                
        #db = mpacdb(self.conf["settings"]["mpac_database"])
        for packageName in packageNameList:
            #first check if package is already installed
            #if db.checkPackageIsInstalled(packageName):
                #self.ErrorMessage.append(packageName+" is already installed")
                #return False
            
            #first extract the package build archive
            if tar.ExtractAllFiles(self.conf["settings"]["AUR_path"]+packageName+".tar.gz", self.conf["settings"]["AUR_path"])==False:
                self.ErrorMessage.append("Error extracting %s"%packageName)
                return False
            #Now read the package build
            if os.path.exists(self.conf["settings"]["AUR_path"]+packageName+"/PKGBUILD"):
                #open the PKGINFO
                pkgInfo=open(self.conf["settings"]["AUR_path"]+packageName+"/PKGBUILD","r")
                lines=pkgInfo.readlines()
                meta=MetaData()
                for line in lines:
                    if line.startswith("pkgname"):
                        meta.name=self.__getValue(line,None)
                    elif line.startswith("pkgver"):
                        meta.version=self.__getValue(line,None)
                    elif line.startswith("pkgdesc"):
                        meta.description=self.__getValue(line,None)
                    elif line.startswith("url"):
                        meta.url=self.__getValue(line,None)
                    elif line.startswith("pkgrel"):
                        meta.pkgRelease=self.__getValue(line,None)    
                    elif line.startswith("arch"):
                        meta.arch=self.__getValue(line, " ")
                        archError=True
                        for arch in meta.arch:
                            if arch == "any" or arch==self.conf["settings"]["architecture"]:
                                archError=False #found match to architecture
                        if archError:                                        
                            self.ErrorMessage.append(meta.name+" ERROR: \n"+arch+" is not supported on this system")
                            return False
                            #raise Exception(meta.name+" ERROR: \n"+arch+" is not supported on this system")
                    elif line.startswith("license"):
                        meta.license=self.__getValue(line," ")
                    elif line.startswith("depends"):
                        meta.depends=self.__getValue(line," ")
                        print(meta.depends) #debug only ******
                    elif line.startswith("conficts"):
                        meta.conflicts=self.__getValue(line," ")
                    elif line.startswith("provides"):
                        meta.provides=self.__getValue(line," ")
                    elif line.startswith("replaces"):
                        meta.replaces=self.__getValue(line)   
                
            self.packageMetaList.append(meta)
            #debug only!!!!!!!!*********************
            #print(meta.depends)
            #print (meta.arch)
            #print(meta.name)
            #print(meta.version)
                    
        return True
    
    #helper function for reading .PKGBUILD - uses list split to split the value (i.e. part after '=')
    def __getValue(self,lineIn,listSplit):
        lineIn=lineIn.replace("(","")
        lineIn=lineIn.replace(")","")
        lineIn=lineIn.replace("'","")
        lineIn=lineIn.replace('"',"")
        linesplit=lineIn.split("=")
        if listSplit != None:
            #split string into list
            tempList=[]
            items=linesplit[1].split(listSplit)
            for item in items:
                if len(item.strip())>0: #we don't want empty strings in the list!
                    tempList.append(item.strip())
            return tempList     
        
        else:
            #return as string
            if len(linesplit)>1:
                return linesplit[1].strip()
            else:
                return None
            
    def MakePackages(self):        
        self.ErrorMessage=[] #clear any previous errors
        result=True 
        for packageMeta in self.packageMetaList:
            os.chdir(self.conf["settings"]["AUR_path"]+packageMeta.name)
            #if subprocess.call(["makepkg","-dcf",packageMeta.name],stderr=None,shell=False)==0:
            if subprocess.call(["makepkg","--asroot","-dcf",packageMeta.name],stderr=None,shell=False)==0:    
                self.packagesMadeOK.append(packageMeta)
            else:    
                result=False
                self.ErrorMessage.append("Error making "+packageMeta.name)
        return result
                
                