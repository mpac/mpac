'''
Created on 3 Jan 2012

@author: marc
'''
import subprocess
import os
#import glob

#replace tarfile module as the xz format is not currently supported (until python 3.3)

def ListFiles(arcFile):
    fileList = subprocess.check_output(["tar", "-tf", arcFile],stderr=None,shell=False)
    #print(fileList) #**** DEBUG ONLY ******
    fileList=fileList.decode("utf_8").split('\n')
    
    #remove directories and .INSTALL and .PKGINFO
    onlyFiles=[]
    for file in fileList:
        if file[-1:] != '/' and file[0:1] != '.' and len(file)>0:
            #print("adding "+file)
            onlyFiles.append(file)
    return onlyFiles


def InstallPackageFiles(archive,ExtractPath):
    currentPath=os.getcwd()
    os.chdir(ExtractPath)
    #tar.StartInfo.Arguments = " --exclude=etc/*  --ignore-command-error --exclude .PKGINFO --exclude .INSTALL --exclude .CHANGELOG -xf "+archive;
    try:
        #subprocess.check_call(["tar","--exclude=etc/*","--exclude .PKGINFO","--exclude .INSTALL","--exclude .CHANGELOG","-xf",archive],stderr=None,shell=True)    
        os.system("tar --exclude=etc/* --exclude .PKGINFO --exclude .INSTALL --exclude .CHANGELOG -xf "+archive)
        os.chdir(currentPath)
        return True
    except: #subprocess.CalledProcessError:
        return False
    

#get files in /etc folder and .INSTALL file
def GetConfigFiles(archive,ExtractPath):    
    currentPath=os.getcwd()
    os.chdir(ExtractPath)
    #tar.StartInfo.Arguments = "-xf "+archive + " --wildcards 'etc/*' --wildcards 'opt/*etc/*'";
    try:
        #subprocess.call(["tar","-xf",archive,"--wildcards", "etc/*","--wildcards", "opt/*etc/*","--wildcards",".INSTALL"," &> /dev/null"],shell=False,stderr=None)        
        #using os.system as could not seem to use &> /dev/null with subprocess.call()
        os.system("tar -xf "+archive + " --wildcards 'etc/*' --wildcards 'opt/*etc/*' --wildcards '.INSTALL' &> /dev/null")
        #subprocess.check_call(["tar -xf"+archive+" --wildcards 'etc/* --wildcards 'opt/*etc/*'"],stderr=None,shell=True,stdout=subprocess.PIPE)    
        os.chdir(currentPath)
        return True
    #except subprocess.CalledProcessError:
    except:
        return False    
    
        
def ExtractFiles(archive, ExtractPath, wildcard):
    #print("ARCHIVE: "+archive)
    #print("EXTRACT PATH: "+ExtractPath)
    currentPath=os.getcwd()
    os.chdir(ExtractPath)
    try:
        subprocess.check_call(["tar", "-xf",archive, "--wildcards",wildcard],stderr=None,shell=False)
        os.chdir(currentPath)
        #print(fileList) #**debug only *********
        return True
    except subprocess.CalledProcessError:
        return False    

    
def ExtractAllFiles(archive, ExtractPath):
    currentPath=os.getcwd()
    os.chdir(ExtractPath)
    try:
        subprocess.check_call(["tar","-xf",archive],stderr=None,shell=False)
        #print(fileList) #**debug only *********
        os.chdir(currentPath)
        return True
    except subprocess.CalledProcessError:
        return False        
    