from TermFunc import printTerm
from TermFunc import pflags
from database import mpacdb

def DoPurge(conf,packageList,options):
    db=mpacdb(conf["settings"]["mpac_database"])
    attrib=pflags()
    for package in packageList:
        if db.checkPackageIsInstalled(package):
            printTerm(attrib.COL_MAIN_BODY, ">> Removing %s from database...."%package,attrib.bold|attrib.noNewLine)
            db.removePackageFiles(package)
            db.removePackage(package)
            printTerm(attrib.COL_OK,"completed", attrib.rightMargin|attrib.bold)
        else:
            printTerm(attrib.COL_ERR_WARN,"%s is not installed"%package,attrib.bold)            
    db.close()