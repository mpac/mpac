'''
Created on 17 Oct 2011

@author: marc
'''
from database import mpacdb
from MetaData import MetaData
from collections import namedtuple
from util import GetOptDepsListFromString

class PackageBase(object):
    
    def __init__(self,conf):
        self.conf=conf
        
    
    '''----------------------------------------------------------
    Helper function look for name in meta.name
    --------------------------------------------------------'''
    def IsNameInMetaList(self,listIn,name):
        result=False
        for item in listIn:
            if item.name==name:
                result=True
                break
        return result


    '''---------------------------------------------------------------
    Helper function look for name in meta.name and return the index
    in list
    ---------------------------------------------------------------'''
    def GetIndexOfNameInMetaList(self,listIn,name):
        result=False
        index=0
        for item in listIn:
            if item.name==name:
                result=True
                break
            index+=1
        
        if result:
            return index
        else:
            return -1    
        
        
        
    '''---------------------------------------------------------------
    Helper find meta.name in list and remove it
    ---------------------------------------------------------------'''        
    def RemoveMetaNameFromList(self,name,listIn):
        for meta in listIn[:]: #iterates over a copy of the list
            if meta.name==name:
                listIn.remove(meta)
                break
                
                
    
    '''--------------------------------------------------------------
    Get provider package for given name.
    Return (provPackage()) where: 
        provPackage=(<providerName>,<version>))
    
    note: package provided can have more than one provider!
    
    n.b. it seems if provider packages have a version it will always be
    using '=' rather than '>' '<' etc.. so I will split on '='
    
    source can be 'installed'  or 'repo'    
    -------------------------------------------------- ------------'''    
    def CheckProviderPackage(self,checkName,source):
        db=mpacdb(self.conf["settings"]["mpac_database"])
        provPackage=[]
        if source=="installed":
            rows=db.getProviderInstalledPackageFor(checkName)
            #print("CHECK INSTALLED") #debug
        if source=="repo":
            rows=db.getProviderRepoPackageFor(checkName)
            #print("CHECK REPO") #debug
                
        #row[0]=checkName
        #row[1]=<provides>=<version>
        if(rows!=None):
            for row in rows: #search through the provides to find the one we are looking for
                provList=row[1].split(" ") #the providers are separated with spaces
                #print("PROV LIST = "+str(provList))
                for provItem in provList:
                    provSplit=provItem.split("=")
                    if(len(provSplit)==1):
                        version=None
                    else:
                        version=provSplit[1]    
                    if checkName==provSplit[0]:
                        #n.b. row[0] is the provider that provides provSplit[0]
                        provPackage.append((row[0],version)) 
            db.close()
            #print("PROV PACKAGE = "+str(provPackage))#debug
            if provPackage:
                return provPackage
            else:
                return None        
        else:    
            db.close()
            return None
    
    '''--------------------------------------------------
    Split out name version and operator from a dependency 
    string
    --------------------------------------------------'''
    def getDepInfo(self,dependency):
        name=None
        operator=None
        version=None
    
        if dependency.find(">=")!=-1:
            operator=">="
            info = dependency.split(">=")
            name=info[0]
            version=info[1]
        elif dependency.find(">=")!=-1:
            operator=">="
            info = dependency.split(">=")
            name=info[0]
            version=info[1]        
        elif dependency.find(">")!=-1:
            operator=">"
            info = dependency.split(">")
            name=info[0]
            version=info[1]        
        elif dependency.find("<")!=-1:
            operator="<"
            info = dependency.split("<")
            name=info[0]
            version=info[1]  
        elif dependency.find("=")!=-1:
            operator="="
            info = dependency.split("=")
            name=info[0]
            version=info[1]  
        else:
            name=dependency
            
        depinfo=namedtuple("depinfo","name operator version")
        depinfo.name=name
        depinfo.operator=operator
        depinfo.version=version    
        
        return (depinfo)    
    
    '''-------------------------------------------------------
    Compare version1 string to version2 string
    -------------------------------------------------------'''
    def CompareVersions(self,versionString1, versionString2):
        
        #format the version strings
        
        #3.2.4-1 is considered equal to 3.2.4 - that is what Arch seems to allow!
        versionString1=versionString1.split("-")[0]
        versionString2=versionString2.split("-")[0]
        
        versionString1=self.__FormatVersionString(versionString1)
        versionString2=self.__FormatVersionString(versionString2)
        
        #now split the decimal points to equalise string
        #will pad left '0' 
        version1Split=versionString1.split(".")
        version2Split=versionString2.split(".")
        
        
        listLength=len(version1Split) #initially assume equal number of elements
        if len(version1Split)>len(version2Split):
            listLength=len(version2Split)
        elif len(version2Split)>len(version1Split):    
            listLength=len(version1Split)
            
        
        #compare lengths of the elements and pad left with 0's
        for i in range(listLength):
            if len(version1Split[i]) > len(version2Split[i]):
                version2Split[i] = version2Split[i].zfill(len(version1Split[i]))
            elif len(version2Split[i]) > len(version1Split[i]):
                version1Split[i] = version2Split[i].zfill(len(version2Split[i]))
        
        #now rebuild
        versionString1=""
        versionString2=""
        for partNum in version1Split:
            versionString1+=partNum
                    
        for partNum in version2Split:
            versionString2+=partNum
        
        #print("VERSION 1 STRING "+versionString1) #debug
        #print("VERSION 2 STRING "+versionString2) #debug
        if versionString1<versionString2:
            #print ("version1 is less than version2") #debug
            return -1
        elif (versionString1>versionString2):
            #print ("version1 is greater than version2") #debug
            return 1
        else:
            #print ("version1 is equal to version2") #debug
            return 0 #equal
             
                

       
    def __FormatVersionString(self,versionString):
        versionString=versionString.strip()        
        #replace - with decimal point
        versionString=versionString.replace("-",".")
        versionString=versionString.lower()
        
        #remove illegal characters, replace with spaces
        versionString=versionString.translate(str.maketrans("abcdefghijklmnopqrstuvwxyz_-*%!^","                                "))
        
        #remove spaces
        versionString=versionString.replace(" ","")
        
        return  versionString
    
 
    
    '''--------------------------------------------------------------
    Convert the raw Db result from sync table into useful meta data
    ---------------------------------------------------------------'''
    def DbSyncResultToMetaData(self,row):
        meta=None
        if(row != None):
            meta=MetaData()
            meta.name=row[0]
            #print("NAME = "+row[0])
            meta.version=row[1]
            meta.description=row[2]
            meta.groups=row[3].split()
            meta.depends=row[4].split()
            meta.optionalDependencies=GetOptDepsListFromString(row[5])
            #print("CONFLICTS = "+row[6])
            meta.conflicts=row[6].split()
            meta.replaces=row[7].split()
            meta.provides=row[8].split()
            meta.url=row[9]
            meta.isize=row[10]
            meta.csize=row[11]
            meta.arch=row[12]
            meta.license=row[13].split()
            meta.distro=row[14]
            meta.repository=row[15]
            meta.packageFileName=row[16]                         
        return meta
    
    
    
    '''------------------------------------------------------------------
    Convert the raw Db result from packages table into useful meta data
    ------------------------------------------------------------------'''
    def DbInstalledToMetaData(self,row):       
        meta=None
        #print("ROW: "+str(row))
        if(row != None):        
            meta=MetaData()
            meta.name = row[0]
            meta.version=row[1]
            meta.description=row[2]
            meta.groups=row[3].split()
            meta.depends=row[4].split()
            meta.optionalDependencies=GetOptDepsListFromString(row[5])
            meta.requiredBy=row[6].split()
            meta.conflicts=row[7].split()
            meta.replaces=row[8].split()
            meta.provides=row[9].split()
            meta.url=row[10];
            meta.installScript=row[11]
            meta.installReason=row[12]
            meta.installDate=row[13]
            meta.isize=row[14]
            meta.arch=row[15]
            meta.license=row[16].split()
            meta.distro=row[17]
            meta.repository=row[18]
        return meta    
    