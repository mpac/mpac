'''
Created on 10 Feb 2012

@author: marc
'''
from PackageBase import PackageBase
from database import mpacdb

class PackageUpgrader(PackageBase):
    
    def __init__(self, conf,options):
        '''
        Constructor
        '''
        self.conf=conf
        self.options=options
        self.packageList=[]
        self.verboseMessages=[]
        
        PackageBase.__init__(self, conf)  #super(PackageBase, self,conf).__init__()
    
    
    '''
    Get a list of names of all the packages that are upgradable
    '''
    def GetUpgradablePackages(self):
        self.packageList=[]
        db=mpacdb(self.conf["settings"]["mpac_database"])
        rows=db.getAllPackagesNameAndVersion()
        for row in rows:
            #row[0]=name row[1]=version
            #find in repo
            repoRow=db.getPackageFromSyncTable(row[0])
            if repoRow != None:
                repoMeta=self.DbSyncResultToMetaData(repoRow)
                #compre versions
                if self.CompareVersions(repoMeta.version, row[1])==1:
                    self.packageList.append(row[0]) # add to list for upgrade
            else:        
                if self.options.verbose:
                    #these would include AUR and local packages
                    #unless I have added code to add these type of packages to the sync table!!!
                    self.verboseMessages.append("Could not find %s in repository"%row[0])
        db.close()   