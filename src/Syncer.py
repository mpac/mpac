'''
Created on Feb 2, 2012

@author: marc
'''
from PackageBase import PackageBase
import tar
import os
import util
from MetaData import MetaData
from database import mpacdb
from collections import namedtuple

class Syncer(PackageBase):

    def __init__(self,conf,options):
        
        self.conf = conf
        self.options=options
        self.__MetaRead=[]
        self.newSyncPackages={}
        self.verboseMessages=[]
        self.errorMessages=[]
        self.normalMessages=[]
        PackageBase.__init__(self, conf)  #super(PackageBase, self,conf).__init__()
        
        
        
    def GetRepositories(self,reposToSync):
        #download repositories
        for repo in reposToSync:
            #unpack
            unpackPath=self.conf["settings"]["download_sync_path"]+repo
            if not os.path.exists(unpackPath):
                os.mkdir(unpackPath) 
            else:
                #make sure the unpack path is empty
                util.emptyDirectory(unpackPath+"/")
            tar.ExtractAllFiles(self.conf["settings"]["download_sync_path"]+repo+".db.tar.gz",unpackPath)
            self.__GetRepoNameVer(repo)            
            #pass control back to calling function all the sync details are stored in global
            #self.newSyncPackages
            
    
    def UpdateDatabase(self,repo):
        self.verboseMessages=[]
        #get all packages for this repo
        db=mpacdb(self.conf["settings"]["mpac_database"])        
        #rows=db.getAllPackagesFromRepo(repo)
        rows=db.getPackagesNameAndVersionFromRepo(repo)
        if rows==None:
            #probably first run - add all packages
            self.__AddAllPackagesToSyncTable(repo)
        else:
            #see what needs updating
            counter=0
            currentSyncPackage=namedtuple('sync_package','name version')
            for row in rows:
                currentSyncPackage.name=row[0]
                currentSyncPackage.version=row[1]
                if currentSyncPackage.name in self.newSyncPackages[repo]: 
                    counter +=1
                    #test version and sync if newer version exists
                    #if self.newSyncPackages[repo][currentSyncPackage.name]>currentSyncPackage.version:    #SUSPECT CODE!!!!!!!!!!!!                
                    if self.CompareVersions(self.newSyncPackages[repo][currentSyncPackage.name], currentSyncPackage.version)==1:
                        #now we need to get all the meta data for the package
                        newSyncMeta=self.__GetMetaData(repo, currentSyncPackage.name+'-'+self.newSyncPackages[repo][currentSyncPackage.name])
                        if newSyncMeta != None:
                            newSyncMeta.repository=repo
                            newSyncMeta.distro="Arch Linux"
                            dbResult= db.updateSyncPackage(newSyncMeta)
                            if  dbResult != True:
                                self.errorMessages.append(dbResult)
                            elif self.options.verbose:
                                self.verboseMessages.append("Synced %s"%currentSyncPackage.name)
                            if counter==500:
                                counter=0
                            db.commit()        
                        else:
                            self.errorMessages.append("Could not get meta data for %s -> %s "%(repo,currentSyncPackage.name))        
                else:
                    #Not found in repository so remove from sync table
                    if self.options.verbose:
                        self.normalMessages.append("Removing %s from sync table (no longer in repository)"%currentSyncPackage.name)
                    rm_result=db.removeFromSyncTable(currentSyncPackage.name,repo)
                    if rm_result != True:
                        self.errorMessages.append(rm_result)
            
            #Now we need to go through the sync dict so see what new packages we need to add to the sync table
            #print(self.newSyncPackages[repo])
            for name in self.newSyncPackages[repo].keys():
                if db.checkPackageInSyncTable(name)==False:
                    #not in sync table so add it
                    repoMeta=self.__GetMetaData(repo, name+'-'+self.newSyncPackages[repo][name])
                    if repoMeta != None:
                        if self.options.verbose:
                            self.verboseMessages.append("adding new package %s to %s"%(name,repo) )
                            #print("adding %s %s"%(name,repo))
                        repoMeta.repository=repo
                        repoMeta.distro="Arch Linux"
                        db.addSyncPackage(repoMeta)
                    else:    
                        self.errorMessages.append("Could not get meta data for new package %s -> %s"%(repo,name))
            db.commit()
            db.close()    
             
        
    '''
    No packages currently exist in the sync table so add all packages retreived.
    (renew table - could be first run!) 
    '''    
    def __AddAllPackagesToSyncTable(self,repo):
        #print("__AddAllPackagesToSyncTable(self,repo) ... TO DO .......")
        if self.options.verbose:
            self.verboseMessages.append("%s is empty will add all packages"%repo)
        db=mpacdb(self.conf["settings"]["mpac_database"])   
        counter=0
        for name in self.newSyncPackages[repo].keys():
            #print("adding %s to %s"%(name,repo)) #debug only **********
            meta=self.__GetMetaData(repo, name+'-'+self.newSyncPackages[repo][name])
            if meta != None:
                meta.repository=repo
                meta.distro="Arch Linux"
                db.addSyncPackage(meta)
                counter+=1
                if counter==500:
                    counter=0
                    db.commit()
        db.commit()        
        db.close()
        
        
    '''
    Build a dictionary of key=name value=version for a given repo. 
    (reads from the file system)
    '''
    def __GetRepoNameVer(self,repo):
        #get directory list for repo
        syncDir=self.conf["settings"]["download_sync_path"]
        dirlist=os.listdir(syncDir+repo)        
        self.newSyncPackages[repo]={}
        for packageName in dirlist:             
            # Now need to get the rest of the meta data from desc file
            descFile = open( syncDir+repo+'/'+packageName+'/desc','r',encoding='utf-8')
            lines = descFile.readlines()          
            self.__readArchMetaFile(lines, '%NAME%', '%', 0)
            for data in self.__metaRead: name=data
            self.__readArchMetaFile(lines, '%VERSION%', '%', 0)
            for data in self.__metaRead: version=data
            
            self.newSyncPackages[repo][name]=version
  
            descFile.close()   
            
        
    
    
    def __GetMetaData(self,repo,name):
        #get directory list for repo
        syncDir=self.conf["settings"]["download_sync_path"]
        repoDir=syncDir+repo+"/"+name+"/"        
        #print('REPO DIR : '+repoDir)
        if os.path.exists(repoDir):
            meta=MetaData()
            #Get the depends data            
            depsFile = open( syncDir+repo+'/'+name+'/depends','r',encoding='utf-8')
            lines = depsFile.readlines()     
            idx= self.__readArchMetaFile(lines, '%DEPENDS%', '%', 0)
            for data in self.__metaRead: meta.depends.append(data)
            idx= self.__readArchMetaFile(lines, '%CONFLICTS%', '%', idx)
            for data in self.__metaRead: meta.conflicts.append(data)
            idx= self.__readArchMetaFile(lines, '%PROVIDES%', '%', idx)
            for data in self.__metaRead: meta.provides.append(data)
            idx= self.__readArchMetaFile(lines, '%OPTDEPENDS%', '%', idx)
            for data in self.__metaRead: meta.optionalDependencies.append(data)
            depsFile.close() 
            
            # Now need to get the rest of the meta data from desc file
            descFile = open( syncDir+repo+'/'+name+'/desc','r',encoding='utf-8')
            lines = descFile.readlines()         
            idx= self.__readArchMetaFile(lines, '%FILENAME%', '%', 0)
            for data in self.__metaRead: meta.packageFileName=data
            idx= self.__readArchMetaFile(lines, '%NAME%', '%', 0)
            for data in self.__metaRead: meta.name=data
            idx= self.__readArchMetaFile(lines, '%VERSION%', '%', 0)
            for data in self.__metaRead: meta.version=data
            idx= self.__readArchMetaFile(lines, '%DESC%', '%', 0)
            for data in self.__metaRead: meta.description=data
            idx= self.__readArchMetaFile(lines, '%GROUPS%', '%', 0)
            for data in self.__metaRead: meta.groups.append(data)
            idx= self.__readArchMetaFile(lines, '%CSIZE%', '%', 0)
            for data in self.__metaRead: meta.csize=data
            idx= self.__readArchMetaFile(lines, '%ISIZE%', '%', 0)
            for data in self.__metaRead: meta.isize=data
            idx= self.__readArchMetaFile(lines, '%MD5SUM%', '%', 0)
            for data in self.__metaRead: meta.md5sum=data
            idx= self.__readArchMetaFile(lines, '%URL%', '%', 0)
            for data in self.__metaRead: meta.url=data
            idx= self.__readArchMetaFile(lines, '%LICENSE%', '%', 0)
            for data in self.__metaRead: meta.license.append(data)
            idx= self.__readArchMetaFile(lines, '%ARCH%', '%', 0)
            for data in self.__metaRead: meta.arch=data
            idx= self.__readArchMetaFile(lines, '%REPLACES%', '%', idx)
            for data in self.__metaRead: meta.replaces.append(data)
        
            #meta.packageURL=repoURL+'/'+meta.fileName                
            #print 'finshed meta'                      
            descFile.close()   
            
            return meta                
        else:
            return None
        
    #got this from the old mpac version so maybe not that elegant"""""
    def __readArchMetaFile(self,linesInFile,searchField,endDelimiter,currentIndex):
        idx = currentIndex
        GotEnd = False
        numlines = len(linesInFile)
        self.__metaRead=[]
        while (idx < numlines) and (GotEnd==False):
            line = linesInFile[idx].strip()
            if len(line) >0:
                if line==searchField: # Found the search field so read until end delimiter is found               
                    idx = idx+1 
                    while (idx < numlines) and (linesInFile[idx][0:1] != endDelimiter) and (GotEnd==False): 
                        if len(linesInFile[idx].strip())>0:
                            self.__metaRead.append(linesInFile[idx].strip())  
                        if linesInFile[idx][0:1] == endDelimiter:
                            GotEnd=True
                        idx = idx+1                            
            idx = idx +1
                   
        
        if GotEnd==False:
            return currentIndex # if not found then return the original index value
        else:
            return idx
            
        