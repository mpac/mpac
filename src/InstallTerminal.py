'''
Created on 19 Jan 2012

@author: marc
'''


#from LogMessage import log
from database import mpacdb
import UserInput
from PackageInstaller import PackageInstaller
from PackageBase import PackageBase
from PackageRemover import PackageRemover
from TermFunc import printTerm
from TermFunc import pflags

#--------------------------------------
#utilise the installer class 
#--------------------------------------
def DoInstall(conf,packageList,options):  
    #will go through each public function (stage) of the InstallPackage class
    Continue=True
    attrib=pflags()
    #printTerm("dark gray","DO the install sucker............",attrib.bold)
    #check packages exist in repository    
    if not options.local:
        missingPackages=[]
        Continue = CheckPackagesExist(packageList,conf,missingPackages)
        if Continue==False:
            #package does not exist but check group before giving up            
            db=mpacdb(conf["settings"]["mpac_database"]) 
            for missingPackage in missingPackages:       
                printTerm(attrib.COL_ERR_WARN,"Could not find %s"%missingPackage,attrib.bold)        
                packageList.remove(missingPackage) #remove from package list 
                #print("Checking groups for ",missingPackage)
                printTerm(attrib.COL_MAIN_BODY, ">> Checking groups for "+missingPackage,attrib.bold)
                groupResults=db.getGroupPackageNames(missingPackage)
                if groupResults==None:
                    printTerm(attrib.COL_ERR_WARN, "No group for "+missingPackage,attrib.bold)
                else:
                    printTerm(attrib.COL_MAIN_BODY, ">> Found group for "+missingPackage,attrib.bold)
                    
                    for name in groupResults:
                        packageList.append(name[0])
            db.close()    
            if len(packageList)>0:
                Continue=True
                
    #resolve dependencies
    if Continue:
        #Call Package Installer Class which will call the resolve dependencies class
        printTerm(attrib.COL_INFO, ">> Resolving dependencies....", attrib.bold)
        installer = PackageInstaller(conf,options)
        installer.packageNameList=packageList #set packages to install
        
        #now call installer.ResolveAll()
        installer.ResolveAll() #note: this also gets any provider packages needed for dependencies
        
        if len(installer.unresolved)>0: 
            #there were unresolved dependencies so abort
            print()
            printTerm(attrib.COL_ERR_WARN, ">> Unresolved....",attrib.bold)
            for unresolv in installer.unresolved:
                #log("  >> "+unresolv +" has unresolved dependencies")
                printTerm(attrib.COL_ERR_WARN, "  >> "+unresolv +" has unresolved dependencies",attrib.bold)
                for depInfo in installer.unresolved[unresolv]:
                    printTerm(attrib.COL_ERR_WARN,"            %s"%(depInfo.name), attrib.bold)
            Continue=False
                    
            
    #check if any of the packaes we are about install replace any installed packages
    #or any others in the install list
    packagesToRemove=[]
    if Continue:
        replaceDict=installer.CheckReplacementPackages()
        if replaceDict != None:
            #we have packages that can be replaced
            #ask user what to do
            #the new package could replace more the one package so for each answer add or remove the package accordingly.
            #as a package may get removed than added again depending on next answer - easier said than done as 
            #installer expects meta data !!!!!!!
            for pkg in replaceDict:
                choice=UserInput.GetYesNoChoice("Replace "+pkg+" with "+replaceDict[pkg])
                if choice=="y":
                    #add package to removalList - check first the it is not in the install list
                    #may need to sense check - if the package to remove is in the install list!!!!
                    packagesToRemove.append(pkg)
                else:
                    #user keeping the old package so remove the new one from the install list
                    idx=installer.GetIndexOfNameInMetaList(installer.packageMetaList, replaceDict[pkg])
                    if idx >=0:
                        installer.packageMetaList.remove(installer.packageMetaList[idx])

        
    #check for package conflicts - if the conflict is in PackagesToRemove then ignore it!
    if Continue:
        printTerm(attrib.COL_INFO, ">> Checking package conflicts....",attrib.bold|attrib.noNewLine)
        if installer.CheckPackageConflicts(packagesToRemove):
            print()
            #package conflicts found, if the conflicts are in the packages to remove list then ignore.
            printTerm(attrib.COL_ERR_WARN, ">> Conflicts found", attrib.bold)
            for conflictPkg in installer.conflictPackages:
                for conflictsWith in installer.conflictPackages[conflictPkg]:
                    #log("  "+conflictPkg+" conflicts with "+conflictsWith)
                    printTerm(attrib.COL_ERR_WARN,"  "+conflictPkg+" conflicts with "+conflictsWith,attrib.bold)
            Continue=False
            #display conflicts to user
        else:
            printTerm(attrib.COL_OK,"completed", attrib.rightMargin|attrib.bold)
            
    
    
    #Now display the details to the user 
    if Continue:
        if len(installer.packageMetaList)>0:
            displayPackages(installer)
            PackagesTotal = len(installer.packageMetaList)+len(installer.newPackages)
            if UserInput.GetYesNoChoice("Install %i Packages"%PackagesTotal)!="y":
                Continue=False
        else:
            #log("No packages to install")
            printTerm(attrib.COL_MAIN_BODY,">> No packages to install",attrib.bold)
            Continue=False        
        
    if Continue:
        installer.setInstallOrder()
        #download packages.......
        if options.local:
            #if the local package is not already in the mpac pkg folder then copy it there
            installer.CopyLocalPackages()
        
        #downloading from repositories
        downloadResult = installer.DownloadPackages()
        if downloadResult != True:
            #log(downloadResult)
            printTerm(attrib.COL_ERR_WARN,downloadResult,attrib.bold)
            Continue=False
            
    if Continue:
        #check for file conflicts
        #log("\n checking for file conflicts...")
        printTerm(attrib.COL_INFO,">> checking for file conflicts....",attrib.bold|attrib.noNewLine)
        if installer.CheckFileConflicts(packagesToRemove):
            print()
            Continue=False
            printTerm(attrib.COL_ERR_WARN,">> File conflcits found",attrib.bold)
            for line in installer.fileConflictInfo:
                printTerm(attrib.COL_ERR_WARN,line, attrib.bold, )
        else:
            printTerm(attrib.COL_OK,"completed", attrib.rightMargin|attrib.bold)
            
    
    #Check if we need to remove any packages first
    if Continue:
        if packagesToRemove:            
            Continue=removePackages(packagesToRemove,conf,options)
    
    #Install packages
    installError=False
    if Continue:           
        for package in installer.packageMetaList:
            printTerm(attrib.COL_MAIN_BODY,">> Installing %s-%s...."%(package.name,package.version),attrib.bold)
            result = installer.InstallPackage(package)
            if result != True:
                installError=True
                printTerm(attrib.COL_ERR_WARN,result, attrib.bold)
    
    #We now need to update the required by fields for all the packages we have installed
    if Continue:
        result=installer.UpdateRequiredBy()
        
    #log("Install Completed")
    if Continue:
        if installError:
            printTerm(attrib.COL_ERR_WARN,"Install completed with Errors", attrib.bold)    
        else:    
            printTerm(attrib.COL_MAIN_BODY,"Install completed", attrib.bold)    
    
    #display the optional dependencies (if any) that installer has compiled.
    if Continue:
        if not installError:
            if len(installer.optionalDepsDict)>0:
                print()
                #db=mpacdb(conf["settings"]["mpac_database"])
                #x=0
                for package in installer.optionalDepsDict.keys():                    
                    printTerm(attrib.COL_HEADER,"Optional Dependencies for %s"%package,attrib.bold)
                    for optDep in installer.optionalDepsDict[package]:
                        #x=x+1
                        #print(x)
                        printTerm(attrib.COL_INFO,"  %s"%optDep,attrib.bold)
                    
            
    return Continue
    


'''------------------------------------------------------------
Display installation details to user.
------------------------------------------------------------'''
def displayPackages(installer):
    userPackagesTotal = len(installer.packageMetaList)
    dependencyTotal = len(installer.newPackages)
    installedSize=0
    downloadSize=0
    totalPackages = userPackagesTotal+dependencyTotal    
    attrib=pflags()
    printTerm(attrib.COL_HEADER,"\nTotal Packages "+str(totalPackages),attrib.bold)
    printTerm(attrib.COL_HEADER,"User Selected Packages ("+str(userPackagesTotal)+")",attrib.bold)
    for userMeta in installer.packageMetaList:
        downloadSize=downloadSize+int(userMeta.csize)
        installedSize=installedSize+int(userMeta.isize)
        printTerm(attrib.COL_MAIN_BODY,"  "+userMeta.name+"-"+userMeta.version, attrib.bold|attrib.noNewLine)
        if userMeta.isUpgrade:
            printTerm(attrib.COL_UPGRADE,"[upgrade]", attrib.rightMargin| attrib.bold)           
        elif userMeta.isReinstall:
            printTerm(attrib.COL_REINSTALL,"[reinstall]", attrib.rightMargin|attrib.bold)
        else:
            printTerm(attrib.COL_NEW,"[new]", attrib.rightMargin|attrib.bold)

    print()
    if len(installer.newPackages)>0:
        printTerm(attrib.COL_HEADER,"Dependencies ("+str(dependencyTotal)+")", attrib.bold)
        for userMeta in installer.newPackages:
            downloadSize=downloadSize+int(userMeta.csize)
            installedSize=installedSize+int(userMeta.isize)
            printTerm(attrib.COL_MAIN_BODY,"  "+userMeta.name+"-"+userMeta.version,attrib.bold|attrib.noNewLine)
            if userMeta.isUpgrade:
                printTerm(attrib.COL_UPGRADE,"[upgrade]", attrib.rightMargin|attrib.bold)
            elif userMeta.isReinstall:
                printTerm(attrib.COL_REINSTALL,"[reinstall]", attrib.rightMargin|attrib.bold)
            else:
                printTerm(attrib.COL_NEW,"[new]", attrib.rightMargin|attrib.bold)

    print()
    downloadSize=downloadSize/1024
    installedSize=installedSize/1024
        
    printTerm(attrib.COL_MAIN_BODY,"Download size: ", attrib.bold|attrib.noNewLine)
    if downloadSize<1024:
        printTerm(attrib.COL_HEADER,format(downloadSize,'.2f')+"KB", attrib.rightMargin|attrib.bold)
    else:
        printTerm(attrib.COL_HEADER,format(downloadSize/1024,'.2f')+"MB", attrib.rightMargin|attrib.bold)
            
    printTerm(attrib.COL_MAIN_BODY,"Installed size: ", attrib.bold|attrib.noNewLine)
    if installedSize<1024:
        printTerm(attrib.COL_HEADER,format(installedSize,'.2f')+"KB", attrib.rightMargin|attrib.bold)
    else:
        printTerm(attrib.COL_HEADER,format(installedSize/1024,'.2f')+"MB", attrib.rightMargin|attrib.bold)
            
            

'''-----------------------------------------------------------------
Get user choice for provider package if more than one avialable
else automatically select it.

will recieve provider dictionary {PackagetoBeProvided: (providerName,version)}
-----------------------------------------------------------------'''
def ProcessProviderPackageList(providerDict,packagesList,conf):   
        
    attrib=pflags()
    for checkPackage in providerDict.keys():
        choice=-1
        db=mpacdb(conf["settings"]["mpac_database"])                    
        providerList=providerDict[checkPackage]
        if db.checkPackageIsInstalled(providerList[0][0])==True: #skip if already installed")
            printTerm(attrib.COL_OK,">> "+providerList[0][0]+" provides "+checkPackage+" (already installed)", attrib.bold)
            packagesList.remove(checkPackage)
            packagesList.append(providerList[0][0])
        else:          
            if len(providerList)==1:
                #only one provider so add automatically
                printTerm(attrib.COL_OK," selected provider package "+checkPackage+" for "+providerList[0][0], attrib.bold)
                choice=0               
            else:
                #user must choose which provider package to use unless the provider package is already in the packages list
                alreadyInList=False
                for providerInfo in providerList:
                    for selPackage in packagesList:
                        if selPackage==providerInfo[0]:
                            alreadyInList=True
                                
                if alreadyInList==False:    
                    print()
                    printTerm(attrib.COL_MAIN_BODY,"Choose package to provide "+checkPackage, attrib.bold)
                    
                    index=0
                    for providerInfo in providerList:
                        printTerm(attrib.COL_VERBOSE,"  >> {0}                      [{1}]".format(providerInfo[0],index+1),attrib.bold)
                        index+=1               
        
                    #get user choice
                    choice = UserInput.GetNumberChoice("", index)
                    choice -= 1 #zero based index
            
                    #check provider version is Ok
                    #if providerList[1]!=None:
                    #if pBase.CompareVersions(checkVersion, providerList[1])<0: #check version is satisfied
                    #raise MpacExceptions.ProviderVersionError(checkPackage,checkVersion,providerList[1])
            
            #replace existing package name with new provider package name 
            if choice!=-1:
                packagesList.remove(checkPackage)
                packagesList.append(providerList[choice][0])
            else:
                #raise MpacExceptions.ProviderProcessingError()
                raise Exception("Error processing provider packages")
            db.close()



'''--------------------------------------------------------------
check packaes are avaiable in sync table, if not check if
the package is provided by another package, else return false
--------------------------------------------------------------'''
def CheckPackagesExist(packageList,conf,missingPackages):
    packagesExist=True
    
    #dict: {'perl-archive-tar': [(), ('perl', '1.76')]} - perl provides perl-archive-tar-1.76
    providerDict={}
        
    pBase=PackageBase(conf)
       
    for packageName in packageList:
        db = mpacdb(conf["settings"]["mpac_database"])
        if(db.checkPackageExists(packageName)==False):                
            db.close()
            #step 2 if package does not exist check for an installed provider package
            provResult=pBase.CheckProviderPackage(packageName, "installed") #check installed providers
            if provResult==None:
                provResult=pBase.CheckProviderPackage(packageName,"repo") #check providers in repositories
                if provResult==None:          
                    missingPackages.append(packageName) #store missing packages for use later on
                    packagesExist=False            
            if packagesExist:
                providerDict[packageName]=provResult    
        db.close()        
    
    #process any provider packages now    
    if(packagesExist):
        if len(providerDict)>0:
            ProcessProviderPackageList(providerDict,packageList,conf) #lets get the users choice
    
    db.close()
    return packagesExist


def removePackages(packagesToRemove,conf,options):
    #print("IN REMOVE PACKAGES!!!!!!!!!!!!!!!!!!!!!!!!!!") #debug only
    #print(packagesToRemove)
    attrib=pflags()
    remover=PackageRemover(conf,options)    
    packagesNotInstalled=[] # not used here but normally used in RemoveTerminal()
    sanityCheck=remover.SanityCheck(packagesToRemove,packagesNotInstalled)
    
    if  sanityCheck != True:        
        #log(sanityCheck)
        printTerm(attrib.COL_ERR_WARN,">> "+sanityCheck,attrib.bold)
        return False

    
    #we don't check conditions for uninstalling as we are replacing the package we are about to remove
    #so just go ahead and uninstall        
    if remover.packagesToRemove:
        for package in remover.packagesToRemove:
            printTerm(attrib.COL_INFO,">> removing "+package, attrib.bold)
            
    
    
    err=remover.RemovePackages()                            
    
            
    if options.verbose:
        for msg in remover.verboseMessages:
            #log(msg)
            printTerm(attrib.COL_VERBOSE,">> "+msg, attrib.bold)
            
    hasError=False
    for msg in err:
        hasError=True
        #log(msg)
        printTerm(attrib.COL_ERR_WARN,">> "+msg, attrib.bold)
            
    if hasError:
        return False

    return True