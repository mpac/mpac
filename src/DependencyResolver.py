'''
Created on 10 Oct 2011

@author: marc
'''

from database import mpacdb
from PackageBase import PackageBase
#from collections import namedtuple
import UserInput
from TermFunc import printTerm
from TermFunc import pflags

#Note: used by PackageInstaller class
class DependencyResolver(PackageBase):   

    def __init__(self,conf,options):
        self.hasError=False
        self.ErrorMessage=""
        self.ExtraPackages=[]
        self.unresolved={}    # <BasePackageName>:<depinfo> - depinfo is a named tuple
        self.conf=conf
        self.options=options
        
        PackageBase.__init__(self,conf)    
            
    def ResolveDependencies(self,packageMetaList):
        #set the package list select by user - needed to check if user 
        #has already selected a provider package
        #self.PackageMetaListCopy = packageMetaList[:]
        for packageMeta in packageMetaList:		
            self.__resolve(packageMeta)
            
        #tidy up list remove extraPackages that are already in the original install list
        for meta in packageMetaList:
            index=self.GetIndexOfNameInMetaList(self.ExtraPackages, meta.name)
            if index!=-1:
                del(self.ExtraPackages[index])
        
            
          
    '''--------------------------------------------------------
    RECURSIVE FUNTCION
    resolve dependency for given package meta data
    --------------------------------------------------------'''
    def __resolve(self, packageMeta):
        attrib=pflags()
        if self.options.verbose:
            #log("Resolving "+packageMeta.name) #debug line
            printTerm(attrib.COL_INFO, "  Resolving "+packageMeta.name,attrib.bold)
        for dependency in packageMeta.depends: 
            
            #DEBUG CODE !!!!!!!!!!!!!!!!!1
            #if dependency=="java-environment":
                #pass
            
            if self.options.verbose:
                #log("   Finding "+dependency) # debug line
                printTerm(attrib.COL_INFO, "     Finding "+dependency,attrib.bold)
            if dependency=="None":
                break #none required, so resolved
           
            if self.CheckInstalledPackages(dependency)==False:
                #print("CHECKING "+dependency) #debug
                self.CheckRepoPackages(dependency,packageMeta.name)
                    
                
            
    '''-----------------------------------------------------------------
    Get user choice for provider package if more than one avialable
    else automatically select it.
    will recieve provider dictionary {PackagetoBeProvided: (providerName,version)}
    note: this of input from terminal will need to divise another method to
    hook into for GUI support.
    -----------------------------------------------------------------'''
    def __ProcessProviderPackageList(self,providerDict):   
        
        for checkPackage in providerDict:
            attrib=pflags()
            choice=-1
            db=mpacdb(self.conf["settings"]["mpac_database"])                    
            providerList=providerDict[checkPackage]
            #print("PROVIDER LIST:") #debug
            #print(providerList)
            #print()
            #print("PROVIDER DICT")
            #print(providerDict)
            #print()
            
            if db.checkPackageIsInstalled(providerList[0][0])==True: #skip if already installed
                if self.options.verbose:
                    #log(">> "+providerList[0][0]+" provides "+checkPackage+" (already installed)")
                    printTerm(attrib.COL_OK, ">> "+providerList[0][0]+" provides "+checkPackage+" (already installed)",attrib.bold)
                    #print(self.unresolved[0])
                    #if checkPackage in self.unresolved:
                        #print("remove from unresolved "+checkPackage)
                        #self.unresolved.popitem(checkPackage) 
            else:          
                if len(providerList)==1:
                    #only one provider so add automatically
                    #log("selected provider package "+checkPackage+" for "+providerList[0][0])
                    printTerm(attrib.COL_INFO, "selected provider package "+checkPackage+" for "+providerList[0][0],attrib.bold)
                    choice=0               
                else:
                    #user must choose which provider package to use unless the provider package is already in the packages list
                    allPackages=self.PackageMetaListCopy + self.ExtraPackages #join lists
                    for providerInfo in providerList:
                        if self.IsNameInMetaList(allPackages,providerInfo[0])==True:
                            alreadyInList=True
                            break                    
           
                    if alreadyInList==False:    
                        #log(">> Choose package to provide "+checkPackage)
                        printTerm(attrib.COL_MAIN_BODY,"Choose package to provide "+checkPackage, attrib.bold, )
                        index=0
                        for providerInfo in providerList:
                            #log (">> {0}                      [{1}]".format(providerInfo[0],index+1))
                            printTerm(attrib.COL_INFO,">> {0}                      [{1}]".format(providerInfo[0],index+1),attrib.bold)
                            index+=1               
        
                        #get user choice
                        choice = UserInput.GetNumberChoice("", index)
                        choice -= 1 #zero based index
            
                #replace existing package name with new provider package name 
                if choice!=-1:
                    row=db.getPackageFromSyncTable(providerList[choice][0])
                    if row != None:
                        self.ExtraPackages.append(self.DbSyncResultToMetaData(row))
                    else:
                        #raise MpacExceptions.ProviderProcessingError()
                        raise Exception("Error processing provider packages") 
                    
                else:
                    #raise MpacExceptions.ProviderProcessingError()
                    raise Exception("Error processing provider packages")
                
                db.close()
 
 
    
    '''--------------------------------------------
    Check installed packages for given dependency
    ---------------------------------------------'''
    def CheckInstalledPackages(self,dependency):
        #Check to see if the dependency is already installed
        attrib=pflags()
        result=True
        depInfo=self.getDepInfo(dependency)
        db = mpacdb(self.conf["settings"]["mpac_database"])
        row=db.getInstalledPackage(depInfo.name)
        #print("CHECKING INSTALLED FOR "+depInfo.name) #debug
        db.close()        
        
        if row != None:
            #package is installed so we need to check if the version is OK
            installedPackage=self.DbInstalledToMetaData(row)
            #print("INSTALLED VERSION IS "+installedPackage.version) #debug
            if self.__IsDependencySatisfied(depInfo,installedPackage.version):
                #dependency is satisfied
                if self.options.verbose:
                    #log(depInfo.name+" is satisfied (installed)") #debug line
                    printTerm(attrib.COL_OK, "     "+depInfo.name+" is satisfied (installed)", attrib.bold)
            else:
                #installed version not satisfied so need to check repositories
                if self.options.verbose:
                    #log(depInfo.name+" is NOT satisfied (installed)") #debug line
                    printTerm(attrib.COL_ERR_WARN, "     "+depInfo.name+" is NOT satisfied (installed)", attrib.bold)
                result=False  
        else:
            result=False  
        return result
    

    '''--------------------------------------------
    Check repositories for a given dependency
    ---------------------------------------------'''    
    def CheckRepoPackages(self,dependency,basePackageName):
        #Check to see if the dependency is available in repositories
        attrib=pflags()
        result=True
        depInfo=self.getDepInfo(dependency)
        db = mpacdb(self.conf["settings"]["mpac_database"])
        row=db.getPackageFromSyncTable(depInfo.name)
        db.close()
        # print("CHECKING REPO************************")
        if row != None:
            #package is in repo so we need to check if the version is OK
            repoPackage=self.DbSyncResultToMetaData(row)
            if self.__IsDependencySatisfied(depInfo,repoPackage.version):
                #dependency is satisfied
                if self.options.verbose:
                    #log(depInfo.name+" is satisfied (repo)") #debug line
                    printTerm(attrib.COL_OK,"     "+depInfo.name+" is satisfied (repo)", attrib.bold)
                if self.IsNameInMetaList(self.ExtraPackages,repoPackage.name)==False:
                    self.ExtraPackages.append(repoPackage) #new package to install
                self.__resolve(repoPackage) #call RECURSIVE FUNCTION
                
            else:
                #repo version not satisfied
                if self.options.verbose:
                    #log(depInfo.name+" is NOT satisfied (repo)") #debug line
                    printTerm(attrib.COL_ERR_WARN,"     "+depInfo.name+" is NOT satisfied (repo)", attrib.bold)
                #add to unresolved dictionary
                if basePackageName in self.unresolved:
                    self.unresolved[basePackageName].append(depInfo) #depInfo is named tuple (no duplicates)
                else:
                    self.unresolved[basePackageName]=[depInfo]
                result=False 
        else:
            #print("will return false")
            if self.options.verbose:
                printTerm(attrib.COL_ERR_WARN,"     "+depInfo.name+" is NOT satisfied (repo)", attrib.bold)
            #we need to check the packge is not provided by another package before giving up!
            providerDict={}
            provResult=self.CheckProviderPackage(depInfo.name, "installed") #check installed providers            
            foundProvider=True
            if provResult==None:
                provResult=self.CheckProviderPackage(depInfo.name,"repo") #check providers in repositories
                if provResult==None: 
                    foundProvider=False            
                    
            if foundProvider:
                #print ("PROV RESULT = "+str(provResult))
                providerDict[depInfo.name]=provResult   
                #process any provider packages now    
                if len(providerDict)>0:
                    self.__ProcessProviderPackageList(providerDict) #lets get the users choice
            if foundProvider == False:
                if basePackageName in self.unresolved:
                    self.unresolved[basePackageName].append(depInfo) #depInfo is named tuple (no duplicates)
                else:
                    self.unresolved[basePackageName]=[depInfo]
                result=False           
        return result
    
    

    
    '''--------------------------------------------------------------------
    Check if depVersion is satisfied by checking against checkVersion
    depending on value of operator.
    Note: depInfo is a tuple (name,operator,version)
    --------------------------------------------------------------------'''    
    def __IsDependencySatisfied(self,depInfo,checkVersion):
        result=None
        if depInfo.version==None:
            result=True
        else:
            #print("CHECK (INSTALLED) VERSION IS "+checkVersion) #debug
            #print("DEPINFO VERSION IS "+depInfo.version) #debug
            compareResult=self.CompareVersions(checkVersion, depInfo.version)
            #print("COMPARE RESULT IS %i"%compareResult)
            if depInfo.operator==">=":
                if compareResult != -1:
                    result=True
                else:
                    result=False                    
            elif depInfo.operator=="<=":
                if compareResult < 1:
                    result=True
                else:
                    result=False
            elif depInfo.operator==">":
                if compareResult == 1:
                    result=True
                else:
                    result=False
            elif depInfo.operator=="<":
                if compareResult == -1:
                    result=True
                else:
                    result=False
            elif depInfo.operator=="=":
                if compareResult == 0:
                    result=True
                else:
                    result=False                            
        return result
                    

    '''----------------------------------------------
    DOES WOT IT SAYS ON THE TIN
    ---------------------------------------------'''
    def GetNewPackages(self):
        return self.ExtraPackages
    
    def GetUnresolved(self):
        return self.unresolved                    
                    
                    
