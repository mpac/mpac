

class ConfigParser:

    def __init__(self,configPath):
        #log 'in ConfigParser __init'
        self.configPath=configPath #Path to the configuration file to parse        
        self.currentSection=''
        self.config={} # place holder for dictionary of sections  to be accessed publically
        self.data = {}
        #log 'about to call __ParseConfig'
        self.__ParseConfig()
        
    #private function
    def __ParseConfig(self):
        #log 'in __ParseConfig'
        #try:
        file = open(self.configPath,'r')
        for line in file.readlines():
            line = line.strip()
            if (len(line)>0) and (line.startswith('#')==False): #comments are '#'
                #log 'line is '+line
                if line.startswith('[') and line.endswith(']'):
                    line = line.replace('[','')
                    line = line.replace(']','')
                    self.__addNewSection(line)
                else:
                    splitline = line.split('=')
                    #check split is correct
                    if (len(splitline) != 2): 
                        #raise MpacExceptions.ConfigParseError
                        raise Exception("Error parsing the config file")                    
                    else:
                        #attributes
                        self.__addToCurrentSection(splitline[0],splitline[1])
                
        #except MpacExceptions.ConfigParseError as m:
        # log (m.message)
            
            
    #Private function
    def __addNewSection(self,section):
        if len(self.currentSection)>0:
            self.config[self.currentSection] = self.data
            #log self.config[self.currentSection]
        self.data={}
        self.currentSection=section #update current section
        
    #Private function
    def __addToCurrentSection(self,attribute,value):
        self.data[attribute]=value
            
        
        
