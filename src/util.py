'''
Created on 6 Jan 2012

@author: marc
'''

import os
import shutil
import hashlib
import constants
#import subprocess
#import glob
#import shlex
#import errno


def emptyDirectory(dirToEmpty):
    if dirToEmpty.endswith("/")==False:
        dirToEmpty = dirToEmpty+"/"
    for item in os.listdir(dirToEmpty):
        if os.path.isdir(dirToEmpty+item):
            shutil.rmtree(dirToEmpty+item) 
        else:
            os.remove(dirToEmpty+item)
            
#is this faster than above????????
'''
def emptyDirectory(dirToEmpty):
    cmd='rm -R '+dirToEmpty+'*'
    arg = shlex.split(cmd)
    arg = arg[:-1] + glob.glob(arg[-1])
    p=subprocess.Popen(arg)
    p.communicate() # wait for exit          
'''
            
def GetMD5(fileIn):
    md5 = hashlib.md5()
    file=open(fileIn,"rb")
    while True:
        data = file.read(128)
        if not data:
            break
        #md5.update(data.encode('iso-8859-1'))
        md5.update(data)
    file.close()    
    return md5.hexdigest().upper() 


#strip operators and return value
def StripOperators(strIn):
    if strIn.find(">=")!=-1:
        operator=">="
    elif strIn.find(">=")!=-1:
        operator=">="
    elif strIn.find(">")!=-1:
        operator=">"
    elif strIn.find("<")!=-1:
        operator="<"
    elif strIn.find("=")!=-1:
        operator="="    
    else:
        return strIn
        
    return strIn.split(operator)[0]


#Get all the files from the root directory
#including sub directories and returns a
#List of the path+file 
def GetAllFilesFromDir(root_dir):
    fileList=[]
    dirLists = os.walk(root_dir,"*","*")
    #note os.walk returns root,dirs,files
    for dirList in dirLists:
        for file in dirList[2]:
            ret_dir=dirList[0]
            if ret_dir.endswith("/")==False:
                ret_dir=ret_dir+"/"
            fileList.append(ret_dir+file)
    return fileList


'''
use os walk to check the destination directories exist,
if not use mkdir to create them. Then use shutil.copy
to copy the files this will overwrite files that already
exist.
'''
def CopyFromWorkingToRootFolder():
    #note os.walk returns root,dirs,files
    dirLists=os.walk(constants.WORKING_FOLDER,"*","*")
    for dirList in dirLists:
        destRoot=dirList[0][len(constants.WORKING_FOLDER):]
        srcDir=dirList[0]
        #print("ROOT IS "+constants.ROOT+dirList[0])
        #print("DEST ROOT IS "+destRoot)
        for directory in dirList[1]:
            #print("checking "+directory)
            if not os.path.isdir(constants.ROOT+destRoot+"/"+directory):
                #print ("    will make directory "+constants.ROOT+destRoot+"/"+directory)
                os.makedirs(constants.ROOT+destRoot+"/"+directory)
                shutil.copystat(srcDir+"/"+directory,constants.ROOT+destRoot+"/"+directory)
        for file in dirList[2]:
            #print("Copy ... "+srcDir+"/"+file)
            #print("to ... "+constants.ROOT+destRoot+"/"+file)
            #copy(srcDir+"/"+file, constants.ROOT+destRoot+"/"+file,overwrite)
            os.system("cp "+srcDir+"/"+file+" "+constants.ROOT+destRoot+"/"+file+" &> /dev/null")
            
            

'''
obsolete - overkill
def copy(src, dst,overwrite):
    if os.path.islink(src):        
        linkto = os.readlink(src)
        
        if overwrite:
            try:
                os.symlink(linkto, dst)            
            except OSError as e:
                if e.errno == errno.EEXIST:
                    os.remove(dst)
            os.symlink(linkto, dst)
                
        else:    
            os.symlink(linkto, dst)
    else:
        #shutil.copy2(src,dst)
        
        #This seems more reliable for copy files that are in use
        os.system("cp %s %s"%(src,dst))        
'''
#Convert list elements to space seperated strings
def GetStringsFromList(listIn):
    result=""
    if len(listIn)==0:
        result="None"
    else:
        if listIn==str:
            result=listIn.strip()
        else:    
            for s in listIn:
                result += s+" "
            result=result.rstrip()
    return result
    
'''-------------------------------------------------------------
Process optional dependencies from string into a list
e.g. return [['langpack: for language'],[video: for video]]
-------------------------------------------------------------'''
def GetOptDepsListFromString(optDeps):
    if optDeps=='None':
        return ['None']
        
    words = optDeps.split(" ")
    line=""
    depList=[]
    firstPass=True
    
    for word in words:
        if(firstPass==False):
            if(word.endswith(":")):
                depList.append(line)
                line=""        
        else:
            firstPass=False
            
        line+=word+" "
          
    depList.append(line)   
    return depList 
                
                
            
            
            
            