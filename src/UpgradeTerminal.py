'''
Created on Feb 9, 2012

@author: marc
'''
from PackageUpgrader import PackageUpgrader
import InstallTerminal
from TermFunc import printTerm
from TermFunc import pflags

def DoUpgrade(conf,options):
    attrib=pflags()
    upgrader=PackageUpgrader(conf,options)
    upgrader.GetUpgradablePackages()
    
    if options.verbose:
        for vMessage in upgrader.verboseMessages:
            printTerm(attrib.COL_ERR_WARN,vMessage,attrib.bold)
            
    if upgrader.packageList:
        InstallTerminal.DoInstall(conf, upgrader.packageList, options)