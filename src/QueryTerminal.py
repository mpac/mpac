'''
Created on May 4, 2012

@author: marc
'''
from TermFunc import printTerm
from TermFunc import printTermIndented
from TermFunc import pflags
from database import mpacdb
from PackageBase import PackageBase
from util import GetStringsFromList
import UserInput
#from optparse import OptionParser

'''
Commands are...
    search <package>                   - search for package (all repos and installed)
        Options:
        --installed -i (search installed only) - DEFAULT
        --repository -r (search packages from repositories)
        --description -d (search description as well as name)
    info <package>                     - get package information
        Options:
        --installed -i (show installed packages only) - DEFAULT
        --repository -r (show packages from repository only)
    owns <path+file>                   - find what package owns the file 
    depends_on <package>                 - get packages that depend on (require) given package 
    list_deps <package>                - list dependencies of given (installed) package
    orphans                            - list orphan (installed) packages (installed as a dependnecy but no longer required by any other package)
    groups <group>
        Options
        --installed -i (search installed groups only)
        --repository -r (search groups from repos only)
    files <package>                    - list files of package (installed)
'''

def DoQuery(conf,command,options,args):
    attrib=pflags()
    
    #parser=OptionParser()
    #parser.add_option("--installed","-i",action="store_true",dest="installed",default=False,help="Only include installed packages")
    #parser.add_option("--description","-d",action="store_true",dest="description",default=False,help="Include description in search")
    #parser.add_option("--repository","-r",action="store_true",dest="repository",default=False,help="Only include data from repositories")
    
    #actually parse the options
    #(options,args) = parser.parse_args(argsIn)
    #print(argsIn)
    
    if command=="search":
        SearchPackages(args,options,conf)
    elif command=="info":
        GetInfo(args,options,conf)
    elif command=="owns":
        WhoOwnsFile(args,conf)
    elif command=="requires":
        GetPackagesThatDependOn(args,conf)
    elif command=="list_deps":
        ListDependencies(args,conf)
    elif command=="orphans":
        GetOrphans(conf)
    elif command=="groups":
        GetGroups(args,conf)
    elif command=="files":
        GetFiles(args,conf)    
    elif command=="aur":
        pass # to do .......    
    else:
        printTerm("red","Invalid query command",attrib.bold)
        

def GetOrphans(conf):
    #Get all packages installed as a dependency
    attrib=pflags()
    packageBase=PackageBase(conf)
    db=mpacdb(conf["settings"]["mpac_database"])
    rows=db.GetAllPackagesInstalledAsDep()
    optDepsList=[]
    orphanList=[]
    if rows is not None:
        for row in rows: 
            isOrphan=False
            #now check package is required by another package
            packageAsDepMeta=packageBase.DbInstalledToMetaData(row)
            depends=db.getPackagesThatDependOn(packageAsDepMeta.name)            
            
            if depends == None:
                #now check optional dependencies
                optDependsRows=db.getPackagesThatOptDependOn(packageAsDepMeta.name)
                if optDependsRows==None:
                    isOrphan=True
                    #check provider packages
                
                    #look at the qpac checkResults handling of optional deps!!!!! 
                
                    if packageAsDepMeta.provides[0]!='None':
                        for provider in packageAsDepMeta.provides:
                            providerInfo=packageBase.getDepInfo(provider)
                            #get the dependencies of the provider package
                            providerDepsRows=db.getPackagesThatDependOn(providerInfo.name)
                            if providerDepsRows==None:
                                #now check optional dependencies of the provider package
                                provOptDependsRows=db.getPackagesThatOptDependOn(providerInfo.name)
                                if provOptDependsRows !=None:
                                    for provOptDependRow in provOptDependsRows:
                                        isOrphan=False;
                                        optDepsList.append("%s is an optional dependency for %s"%(packageAsDepMeta.name,provOptDependRow[0]))
                            else:
                                isOrphan=False         
            if isOrphan:
                orphanList.append(packageAsDepMeta.name)
            
    
    if orphanList:
        printTerm(attrib.COL_HEADER,"Listing orphan packages:",attrib.bold)
        for orphan in orphanList:
            printTerm(attrib.COL_MAIN_BODY,"  %s"%orphan,attrib.bold)
    else:
        printTerm(attrib.COL_MAIN_BODY,">> No orphans found",attrib.bold)        
    
    if UserInput.GetYesNoChoice("View dependencies that are only required optionally?")=="y":
        if optDepsList:
            for optDepInfo in optDepsList:
                printTerm(attrib.COL_INFO,"%s"%optDepInfo,attrib.bold)
        else:
            printTerm(attrib.COL_MAIN_BODY,">> No Optional dependencies found",attrib.bold)
            
    #for optDep in optDepsList    

def GetFiles(args,conf):
    attrib=pflags()
    db=mpacdb(conf["settings"]["mpac_database"])
    for package in args:
        if db.checkPackageIsInstalled(package):
            files=db.getPackageFiles(package)
            if files != None:
                printTerm(attrib.COL_HEADER,"Files for %s"%package,attrib.bold)
                for file in files:
                    printTerm(attrib.COL_MAIN_BODY,"%s"%file,attrib.bold)
            else:
                printTerm(attrib.COL_ERR_WARN,"Could not get files for %s"%package,attrib.bold)
        else:
            printTerm(attrib.COL_ERR_WARN,"%s is not installed"%package,attrib.bold)
    


def GetGroups(args,conf):
    attrib=pflags()
    db=mpacdb(conf["settings"]["mpac_database"])
    for group in args:
        rows=db.getGroupPackageNames(group)
        if rows != None:
            printTerm(attrib.COL_HEADER,"Group %s"%group,attrib.bold)
            for row in rows:
                package=row[0]
                printTerm(attrib.COL_MAIN_BODY,"   %s"%package,attrib.bold|attrib.noNewLine)
                if db.checkPackageIsInstalled(package):
                    printTerm(attrib.COL_OK,"[installed]",attrib.bold|attrib.rightMargin)
                else:
                    print() #new line    
        else:
            printTerm(attrib.COL_ERR_WARN,"Could not find group %s"%group,attrib.bold)
    db.close()        
            

def ListDependencies(args,conf):
    attrib=pflags()
    db=mpacdb(conf["settings"]["mpac_database"])
    packageBase=PackageBase(conf)
    for package in args:
        if db.checkPackageIsInstalled(package):
            row=db.getAllPackageInfo(package)
            meta=packageBase.DbInstalledToMetaData(row)
            if len(meta.depends)>0:
                printTerm(attrib.COL_HEADER,"Listing dependencies of %s"%package,attrib.bold)
                for depend in meta.depends:
                    printTermIndented(attrib.COL_MAIN_BODY,depend,attrib.bold,3)
            else:
                printTerm(attrib.COL_INFO,"No dependencies found for %s"%package,attrib.bold)
            
        else:
            printTerm(attrib.COL_ERR_WARN,"%s is not installed"%package,attrib.bold)
    db.close()
    
        
def GetInfo(args,options,conf):
    attrib=pflags()
    db=mpacdb(conf["settings"]["mpac_database"])
    packageBase=PackageBase(conf)
    for package in args:        
        if options.repository:
            row=db.getPackageFromSyncTable(package)
        else:
            row=db.getInstalledPackage(package)
            print ("here we are......................................")    
        if row != None:
            if options.repository:
                meta=packageBase.DbSyncResultToMetaData(row)
            else:
                meta=packageBase.DbInstalledToMetaData(row)
            printTerm(attrib.COL_HEADER,"Package info for %s"%package,attrib.bold)
            printTermIndented(attrib.COL_MAIN_BODY,"Name:             %s"%meta.name,attrib.bold,3)
            printTermIndented(attrib.COL_MAIN_BODY,"Version:          %s"%meta.version,attrib.bold,3)
            printTermIndented(attrib.COL_MAIN_BODY,"Description:      ",attrib.bold,3)
            printTermIndented(attrib.COL_MAIN_BODY,meta.description,attrib.bold,5)
            printTermIndented(attrib.COL_MAIN_BODY,"Repository:       %s"%meta.repository,attrib.bold,3)
            printTermIndented(attrib.COL_MAIN_BODY,"Group:        ",attrib.bold,3)
            printTermIndented(attrib.COL_MAIN_BODY,GetStringsFromList(meta.groups),attrib.bold,5)
            printTermIndented(attrib.COL_MAIN_BODY,"Dependencies: ",attrib.bold,3)            
            printTermIndented(attrib.COL_MAIN_BODY,GetStringsFromList(meta.depends),attrib.bold,5)
            printTermIndented(attrib.COL_MAIN_BODY,"Opt Dependencies: ",attrib.bold,3)
            printTermIndented(attrib.COL_MAIN_BODY,GetStringsFromList(meta.optionalDependencies),attrib.bold,5)
            printTermIndented(attrib.COL_MAIN_BODY,"Required By: ",attrib.bold,3)
            printTermIndented(attrib.COL_MAIN_BODY,GetStringsFromList(meta.requiredBy),attrib.bold,5)
            printTermIndented(attrib.COL_MAIN_BODY,"Conflicts with: ",attrib.bold,3)
            printTermIndented(attrib.COL_MAIN_BODY,GetStringsFromList(meta.conflicts),attrib.bold,5)
            printTermIndented(attrib.COL_MAIN_BODY,"Replaces: ",attrib.bold,3)
            printTermIndented(attrib.COL_MAIN_BODY,GetStringsFromList(meta.replaces),attrib.bold,5)
            printTermIndented(attrib.COL_MAIN_BODY,"URL:              %s"%meta.url,attrib.bold,3)
            if not options.repository:
                #these fields are only for installed packages
                printTermIndented(attrib.COL_MAIN_BODY,"Install Reason:   %s"%meta.installReason,attrib.bold,3)
                printTermIndented(attrib.COL_MAIN_BODY,"Install Date:     %s"%meta.installDate,attrib.bold,3)
                printTermIndented(attrib.COL_MAIN_BODY,"Installed Size:   %s"%meta.isize,attrib.bold,3)
            else:
                #these fields are for packages from repository (sync table)
                printTermIndented(attrib.COL_MAIN_BODY,"Download Size:    %s"%meta.csize,attrib.bold,3)    
            printTermIndented(attrib.COL_MAIN_BODY,"Architecture      %s"%meta.arch,attrib.bold,3)
            printTermIndented(attrib.COL_MAIN_BODY,"Licenses: ",attrib.bold,3)            
            printTermIndented(attrib.COL_MAIN_BODY,GetStringsFromList(meta.license),attrib.bold,5)
            
        else:
            printTerm(attrib.COL_ERR_WARN,"Could not find %s"%package,attrib.bold)        
    db.close()
        
        
#Get packages that depend on <arg> or in other words packages required by <arg>
def GetPackagesThatDependOn(args,conf):
    attrib=pflags()
    db=mpacdb(conf["settings"]["mpac_database"])
    for package in args:
        rows=db.getPackagesThatDependOn(package)
        if rows==None:
            printTerm(attrib.COL_MAIN_BODY,"No packages depend on %s"%package,attrib.bold)
        else:
            printTerm(attrib.COL_HEADER,"Packages that depend on %s"%package,attrib.bold)
            for row in rows:
                printTermIndented(attrib.COL_MAIN_BODY,"%s-%s"%(row[0],row[1]),attrib.bold,3)
    db.close()  
        
#Get file owner        
def WhoOwnsFile(args,conf):
    attrib=pflags()
    db=mpacdb(conf["settings"]["mpac_database"])
    for file in args:
        owner=db.getFileOwner(file)
        if owner != None:
            printTerm(attrib.COL_INFO,"%s is owned by %s"%(file,owner),attrib.bold)
        else:
            printTerm(attrib.COL_ERR_WARN,"Could not find owner of %s"%file,attrib.bold)
    db.close()        

#Search packages - default options search repos but also shows installed 
#version (if installed). Option --installed will only search installed packages.
def SearchPackages(args,options,conf):
    attrib=pflags()
    db = mpacdb(conf["settings"]["mpac_database"])
    for package in args:
        if options.installed and not options.repository:
            #search only installed packages
            if options.description:
                rows = db.searchInstalled(package,True)
            else:
                rows = db.searchInstalled(package,False)
            if rows != None:
                for row in rows:
                    printTerm(attrib.COL_INFO,"%s-%s"%(row[0],row[1]),attrib.bold)
                    printTermIndented(attrib.COL_MAIN_BODY,row[2]+"\n",attrib.bold,3)
                    
        elif (options.repository and not options.installed): 
            #search only packages from reposoitores
            if options.description:
                rows = db.searchRepos(package,True)
            else:
                rows = db.searchRepos(package,False)
            for row in rows:
                printTerm(attrib.COL_INFO,"%s-%s"%(row[0],row[1]),attrib.bold)
                printTermIndented(attrib.COL_MAIN_BODY,row[2]+"\n",attrib.bold,3)

        else:
            #search repositories - check if package is installed - if so display the installed name and vesion of the package as well
            iRows=None
            if options.description:
                rows = db.searchRepos(package,True)
            else:
                rows = db.searchRepos(package,False)
            if rows != None:
                #is the package installed
                for row in rows:
                    if db.checkPackageIsInstalled(row[0]):
                        #get installed package name and version
                        iRows = db.getInstalledPackage(row[0])
                        printTerm(attrib.COL_INFO,"%s-%s [installed=%s]"%(row[0],row[1],iRows[1]),attrib.bold)
                    else:
                        printTerm(attrib.COL_INFO,"%s-%s"%(row[0],row[1]),attrib.bold)
                
                    printTermIndented(attrib.COL_MAIN_BODY,row[2]+"\n",attrib.bold,3)
    db.close()