'''
Created on 7 Apr 2012

@author: marc
'''

import subprocess
from sys import stdout

'''
colours = {'magenta':';35m', \
'blue':  ';34m', \
'green': ';32m', \
'yellow': ';33m',\
'red': ';31m',\
'cyan': ';36m',\
'black': ';30m', \
'white': ';37m', \
'dark gray':'1;30m',\
'bright gray':  '0;37m', \
'endc': '\033[0m'}
'''

colours = { \
    'black':    '0;30m',     'bright gray':  '1;37m', \
    'blue':     '0;34m',     'bright white': ';37m', \
    'green':    '0;32m',     'bright blue':  '1;34m', \
    'cyan':     '0;36m',     'bright green': '1;32m', \
    'red':      '0;31m',     'bright cyan':  '1;36m', \
    'purple':   '0;35m',     'bright red':   '1;31m', \
    'yellow':   '0;33m',     'bright purple':'1;35m', \
    'dark gray':'1;30m',     'bright yellow':'1;33m', \
    'magenta':'0;35m',       'endc': '\033[0m'}




#bit flags
class pflags():
    def __init__(self):
        self.rightMargin=1
        self.bold=2
        self.noNewLine=4
        '''
        For dark background
        '''
        self.COL_HEADER='magenta'
        self.COL_INFO='bright cyan'
        self.COL_MAIN_BODY='bright white'
        self.COL_ERR_WARN='red'
        self.COL_VERBOSE='yellow'
        self.COL_UPGRADE='bright yellow'
        self.COL_REINSTALL='blue'
        self.COL_NEW='bright green'
        self.COL_OK='green'


def printTerm(colour,text,attributes):
    #print(text) #debug
    #return #debug

    attrib=pflags()
    if attributes & attrib.rightMargin==attrib.rightMargin:
        cols=getColumns()
        pos=cols-len(text)
        setColumn(pos)

    if attributes & attrib.bold==attrib.bold:
        fontType='1'
    else:
        fontType='0'
    
    if attributes & attrib.noNewLine!=attrib.noNewLine:
        print('\33['+fontType+colours[colour]+text+colours['endc'])

    else:    
        #endChar=text[-1:]
        #print('\33['+fontType+colours[colour]+text+colours['endc'],end='') #- this does something strange!!! so using stdout
        stdout.write('\33['+fontType+colours[colour]+text+colours['endc'])
        stdout.flush()
        

# indent and wrap text        
def printTermIndented(colour,text,attributes,indent):
    #format the text block
    text = text.replace("\n"," ")
    text=text.strip()
    words=text.split(' ')
    width=getColumns()
    indentStr=""
    for x in range(indent):
        indentStr=indentStr+" "
    line=""
    #formattedtext=""
    for word in words:
        if len(indentStr+line+" "+word)<=width:
            line=line+" "+word
        else:
            printTerm(colour,indentStr+line,attributes)
            line=""
    
    if len(line)>0:
        printTerm(colour,indentStr+line,attributes)
        

def scrollToEnd():
    lines=int(subprocess.Popen(['tput','lines'],stdout=subprocess.PIPE).stdout.read().decode('utf8'))
    for x in range(lines):
        print(".")
   
        
#Gets the number of colums for the current terminal (does not include current cursor position)     
def getColumns():
    cols=subprocess.Popen(['tput', 'cols'], stdout=subprocess.PIPE).stdout.read().decode('utf8')
    return int(cols)

def setColumn(pos):
    #get current line number
    
    lineNum=int(subprocess.Popen(['tput','lines'],stdout=subprocess.PIPE).stdout.read().decode('utf8'))
    #print("line number is %i"%lineNum)
    #set new column position
    subprocess.call(['tput','cup',str(lineNum),str(pos)],stderr=None,shell=False)
    #subprocess.call(['tput','cuf',str(pos)],stderr=None,shell=False)

